package com.example.application.reuseablecomponent.input;

import com.vaadin.flow.component.Component;
import com.vaadin.flow.component.Composite;
import com.vaadin.flow.component.orderedlayout.FlexLayout;
import com.vaadin.flow.component.orderedlayout.VerticalLayout;
import com.vaadin.flow.component.textfield.PasswordField;

public class ReuseableComponentTemplate extends Composite<Component> {
private static int count = 0;
private FlexLayout flexLayout = new FlexLayout();


@Override
protected Component initContent() {
	flexLayout.setFlexDirection(FlexLayout.FlexDirection.ROW);
	flexLayout.setAlignContent(FlexLayout.ContentAlignment.SPACE_BETWEEN);


	return flexLayout;
}

private FlexLayout getFlexLayout() {
	return flexLayout;
}

private VerticalLayout getVerticalLayout() {
	VerticalLayout verticalLayout = new VerticalLayout();
	verticalLayout.setPadding(true);
	verticalLayout.setMargin(true);
	verticalLayout.getStyle().set("outline", "2px solid");
	return verticalLayout;
}

private void addToFlexLayout(Component passedComponent) {
	this.flexLayout.add(passedComponent);
}


private Component createVerifiedComponent() {
	count++;
	//PasswordField verifiable = createDefaultPasswordField();
	PasswordField verifiable = new PasswordField();
	verifiable.setRequired(true);
	verifiable.setAutofocus(true);
	verifiable.setAutoselect(true);
	verifiable.setMaxLength(25);
	verifiable.setMinLength(14);
	verifiable.setPattern("^[a-zA-Z\\s]+");
	verifiable.setErrorMessage("Crappy password must be upper/lower case characters only");
	setValueChangeListener(verifiable);
	return verifiable;
}

private Component setValueChangeListener(Component verifiable) {
	/*verifiable.addValueChangeListener(event -> {
		if(verifiable.isInvalid()) {
			System.out.println("Password is invalid");
			verifiable.setPlaceholder("Last entry was invalid");
		}
	});*/
	return verifiable;
}

private Component createDefaultComponent() {
	/*count++;
	PasswordField passwordField = new PasswordField();
	passwordField.setClearButtonVisible(true);
	passwordField.setTitle("Title: PasswordField " + count);
	passwordField.setPlaceholder("enter password");
	passwordField.setLabel("Label: PF " + count);*/
	return null;
}

public ReuseableComponentTemplate addBasic() {
VerticalLayout verticalLayout = getVerticalLayout();
verticalLayout.add(
	// get component
);
addToFlexLayout(verticalLayout);
	return this;
}

public ReuseableComponentTemplate addWithVerification() {
	VerticalLayout verticalLayout = getVerticalLayout();
	verticalLayout.add(
		// get component createVerifiedPasswordField()
	);
	addToFlexLayout(verticalLayout);
	return this;
}



public FlexLayout addBasicReturnFlex() {
	VerticalLayout verticalLayout = getVerticalLayout();
	verticalLayout.add(
		// get component createDefaultPasswordField()
	);
	addToFlexLayout(verticalLayout);
	return getFlexLayout(); // this isn't doing anything that the initContent doesn't do already..
}




}
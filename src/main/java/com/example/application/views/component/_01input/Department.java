package com.example.application.views.component._01input;

public class Department {
private Integer id;
private String name;

public Department(Integer id, String name) {
	this.id = id;
	this.name = name;
}

@Override
public String toString() {
	return name;
}
}

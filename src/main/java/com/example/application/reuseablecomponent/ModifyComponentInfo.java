package com.example.application.reuseablecomponent;

import com.vaadin.flow.component.orderedlayout.HorizontalLayout;
import com.vaadin.flow.component.orderedlayout.VerticalLayout;

import javax.annotation.meta.When;

public class ModifyComponentInfo {

/*
When implementing this kind of dynamic UIs, it’s useful to be able to modify the component tree in a layout.
Let’s see some of the methods available in VerticalLayout and HorizontalLayout that will help you to do this.

You can get the components inside a layout as a Java Stream using the getChildren() method :

toolbar.getChildren().forEach(component -> {
  ... do something with component ...
});

Similarly, you can get the parent component using the getParent() method:

toolbar.getParent().ifPresent(component -> {
	CompositionView view = (CompositionView) component;
  ... do something with view ...
});
You can remove individual components or all the contained components using the
remove(Component...) and removeAll() methods, respectively:

var button = new Button();
toolbar.add(button);
toolbar.remove(button); // removes only button
toolbar.removeAll(); // removes all the contained components

When building views dynamically, the replace(Component, Component) method could be useful:
var button = new Button();
toolbar.add(button);
toolbar.replace(button, new Button("New!"));
*/



}

package com.example.application.views.layout;

import com.vaadin.flow.component.button.Button;
import com.vaadin.flow.component.html.Paragraph;
import com.vaadin.flow.component.orderedlayout.VerticalLayout;
import com.vaadin.flow.router.Route;

@Route("09_Vertical-Layout")
public class _09Vertical extends VerticalLayout {
public _09Vertical() {
	add(
	new Paragraph("Paragraph 1 in vertical layout"),
	new Paragraph("Paragraph 1 in vertical layout"),
	new Button("Buttton!")
	);
}
}

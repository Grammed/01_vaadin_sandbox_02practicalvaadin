package com.example.application.reuseablecomponent.input;

import com.vaadin.flow.component.Component;
import com.vaadin.flow.component.Composite;
import com.vaadin.flow.component.datepicker.DatePicker;
import com.vaadin.flow.component.datetimepicker.DateTimePicker;
import com.vaadin.flow.component.notification.Notification;
import com.vaadin.flow.component.orderedlayout.HorizontalLayout;
import com.vaadin.flow.component.timepicker.TimePicker;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.util.Locale;

public class DateTimeInput extends Composite<Component> {
	HorizontalLayout dateTimeHorizontalHolder = new HorizontalLayout();

@Override
protected Component initContent() {

	return dateTimeHorizontalHolder;
}

public HorizontalLayout getTimePicker() {
	TimePicker timePicker = new TimePicker("Pick a time");
	timePicker.addValueChangeListener(event -> {
		LocalTime value = event.getValue();
		Notification.show("Time: " + value);
	});
	this.dateTimeHorizontalHolder.add(
		timePicker
	);
	setDefaultStyle();
	return dateTimeHorizontalHolder;
}

public HorizontalLayout getDateTimePicker() {
	DateTimePicker dateTimePicker = new DateTimePicker("When? ");
	dateTimePicker.addValueChangeListener(event -> {
		LocalDateTime value = event.getValue();
		Notification.show("Time: " + value);
	});
	this.dateTimeHorizontalHolder.add(
		dateTimePicker
	);
	setDefaultStyle();
	return dateTimeHorizontalHolder;
}

public HorizontalLayout getDatePicker() {
	DatePicker datePicker = new DatePicker(
		"Enter A Memorable Date: ",
		LocalDate.now(),
		event -> showMessage(event.getValue())
	);

	this.dateTimeHorizontalHolder.add(
		datePicker
	);
	setDefaultStyle();
	return this.dateTimeHorizontalHolder;
}

public HorizontalLayout getDatePicker(String label) {
	DatePicker datePicker = new DatePicker(
		label,
		LocalDate.now(),
		event -> showMessage(event.getValue())
	);
	this.dateTimeHorizontalHolder.add(
		datePicker
	);
	setDefaultStyle();
	return this.dateTimeHorizontalHolder;
}

public HorizontalLayout getDatePicker(LocalDate min, LocalDate max) {
	DatePicker datePicker = new DatePicker(
		"The shows all methods version",
		min,
		event -> showMessage(event.getValue())
	);

	showSomeMethods(min, max, datePicker);
	this.dateTimeHorizontalHolder.add(
		datePicker
	);
	setDefaultStyle();
	return this.dateTimeHorizontalHolder;
}

private void showSomeMethods(LocalDate min, LocalDate max, DatePicker datePicker) { // not showing all methods but many of them...
	datePicker.setErrorMessage("Date entered must be up to three months before or after today's date.");
	datePicker.setMin(min);
	datePicker.setMax(max);
	datePicker.setPlaceholder("Earliest..");
	datePicker.setInitialPosition(min);// This is not working poss because of min-max being set (set in constructor instead now)
	datePicker.setAutoOpen(false);
	datePicker.setClearButtonVisible(true);
	datePicker.setLocale(Locale.CANADA_FRENCH);
}

private void setDefaultStyle() {
	this.dateTimeHorizontalHolder.getStyle().set("outline", "4px dashed");
	this.dateTimeHorizontalHolder.setPadding(true);
	this.dateTimeHorizontalHolder.setMargin(true);
}

private void showMessage(LocalDate date) {
	Notification.show(
		date + " is great!"
	);
}
}

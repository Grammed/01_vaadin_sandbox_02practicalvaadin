package com.example.application.reuseablecomponent;

import com.vaadin.flow.component.Component;
import com.vaadin.flow.component.Composite;
import com.vaadin.flow.component.button.Button;
import com.vaadin.flow.component.html.Paragraph;
import com.vaadin.flow.component.orderedlayout.HorizontalLayout;
import com.vaadin.flow.component.orderedlayout.VerticalLayout;
import com.vaadin.flow.router.Route;

// Instantiate this anywhere you want it var compositeLayout = new ReuseableComponentStep1
@Route("reuse-component-firstStep") // remove the route
public class ReusableComponentStep1 extends Composite<Component> {

	@Override
protected Component initContent() {
	var toolbar = new HorizontalLayout(
		new Button("Reuseable? 1"),
		new Button("Reuseable? 2"),
		new Button("Reuseable? 3")
	);
	return new VerticalLayout(
		toolbar,
		new Paragraph("Reuseable? 1"),
		new Paragraph("Reuseable? 2"),
		new Paragraph("Reuseable? 3")
	);
}

}

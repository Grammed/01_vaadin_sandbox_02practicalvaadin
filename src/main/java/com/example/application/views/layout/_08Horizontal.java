package com.example.application.views.layout;

import com.vaadin.flow.component.button.Button;
import com.vaadin.flow.component.html.Paragraph;
import com.vaadin.flow.component.orderedlayout.HorizontalLayout;
import com.vaadin.flow.router.Route;

@Route("08_Horizontal-Layout")
public class _08Horizontal extends HorizontalLayout {

public _08Horizontal() {
	add(
		new Button("BasicS"),
		new Paragraph("Horizontal paragraph1"),
		new Paragraph("Horizontal paragraph2")
	);
}
}

package com.example.application.views.component._01input;

import com.example.application.reuseablecomponent.input.*;
import com.example.application.reuseablecomponent.layout_alignment.Annotation;
import com.vaadin.flow.component.checkbox.CheckboxGroup;
import com.vaadin.flow.component.combobox.ComboBox;
import com.vaadin.flow.component.listbox.ListBox;
import com.vaadin.flow.component.listbox.MultiSelectListBox;
import com.vaadin.flow.component.notification.Notification;
import com.vaadin.flow.component.orderedlayout.FlexLayout;
import com.vaadin.flow.component.orderedlayout.HorizontalLayout;
import com.vaadin.flow.component.orderedlayout.VerticalLayout;
import com.vaadin.flow.component.radiobutton.RadioButtonGroup;
import com.vaadin.flow.router.Route;

import java.time.LocalDate;
import java.util.Set;


@Route("01_Input-Components")
public class _01Single_Multi_Input_View extends VerticalLayout {
MockDepartmentServiceSingleton mockDepartmentServiceSingleton = MockDepartmentServiceSingleton.getInstance();
VerticalLayout body = new VerticalLayout();
HorizontalLayout _1textFieldRow = new HorizontalLayout();
HorizontalLayout _2passwordFieldRow = new HorizontalLayout();
HorizontalLayout _3booleanBoxRow = new HorizontalLayout();
HorizontalLayout _4dateTimePickerRow = new HorizontalLayout();
HorizontalLayout _5NumericInput = new HorizontalLayout();
HorizontalLayout _6ComboBoxSingle = new HorizontalLayout();
HorizontalLayout _7RadioButtonSingle = new HorizontalLayout();

Annotation annotation1 = new Annotation().createAndSetText("TEXT FIELD, these are all returned in their own Flexboxes as they are individual instances of TextBoxComponent");
TextFieldComponent textFieldComponent1 = new TextFieldComponent().addBasicTextField();
TextFieldComponent textFieldComponent2 = new TextFieldComponent().addTextField();
TextFieldComponent textFieldComponent3 = new TextFieldComponent().addVerifiedTextField();

Annotation annotation2 = new Annotation().createAndSetText("PASSWORD FIELD, these are all within one flexbox owned by a single instance of PasswordInputComponent, margin has to be added to them the VerticalLayouts they are in");
PasswordFieldComponent passwordFieldComponent1 = new PasswordFieldComponent().addBasic();

Annotation annotation3 = new Annotation().createAndSetText("CHECKBOX components:");
BooleanComponent_1 checkboxComponent1 = new BooleanComponent_1();
Annotation annotation4 = new Annotation().createAndSetText("Up until this point my concerns were all linked to layout, but sending components back hidden in a couple of layouts with no reference means getting data values from them is overly complicated." +
	                                                           "The checkbox to the right is outputting data as it is used, but to get a statecheck on it (true/false) in the class that builds it not a priority, I see, that if the data value" +
	                                                           "were BOUND, it's passing back what it needs to, just like a html component.. It is souting to console.");
BooleanComponent_2 checkboxComponent2 = new BooleanComponent_2();

Annotation annotation5 = new Annotation().createAndSetText("DATE TIME PICKER: (layout used different from above, HorizontalLayout -> FlexLayout -> the HorizontalLayout holding Date Pickers of the same DateTimeInput_1 instance.");
FlexLayout dateTimeFlexRow = new FlexLayout();
DateTimeInput dateTimeInput_Component = new DateTimeInput();
DateTimeInput dateTimeInput_Component2 = new DateTimeInput();

Annotation annotation6 = new Annotation().createAndSetText("NUMERIC INPUT: Allows you to get a Double back from the user. ");
FlexLayout numericFlexRow = new FlexLayout();
NumericInput numericInput_Component1 = new NumericInput();

Annotation annotation7 = new Annotation().createAndSetText("COMBO-BOX SINGLE INPUT: Allows choice from prescribed list. 1. is wrapped and the wrapper is added, 2 is added to wrapper along with 3");
FlexLayout comboSingleFlexRow = new FlexLayout();
SingleSelectCombobox singleSelectCombobox1 = new SingleSelectCombobox();
SingleSelectCombobox singleSelectCombobox2 = new SingleSelectCombobox();
SingleSelectCombobox singleSelectCombobox3 = new SingleSelectCombobox();
Annotation annotation8 = new Annotation().createAndSetText("Collection can be passed in order to name options, Object.toString() is used by default");

Annotation annotation9 = new Annotation().createAndSetText("RADIO-BUTTON GROUP: Single Select");
FlexLayout radioSingleFlexRow = new FlexLayout();
SingleSelectRadioButtonGroup singleSelectRadioButtonGroup1 = new SingleSelectRadioButtonGroup();
SingleSelectRadioButtonGroup singleSelectRadioButtonGroup2 = new SingleSelectRadioButtonGroup();
SingleSelectRadioButtonGroup singleSelectRadioButtonGroup3 = new SingleSelectRadioButtonGroup();

Annotation annotation10 = new Annotation().createAndSetText("1: Created list instantiated in Component Composite," +
	  "2: Created with passed label, then items instantiated and set as items in calling class (view)" +
	  "3: List created in mock Service and passed to Component Composite, 4: LIST-BOX example 5: CHECKBOX-GROUP returns a set 6: MultiSelectLISTBOX");


public _01Single_Multi_Input_View() {

	// region ADD COMPONENT ROW TO BODY
	add(
		body
	);

	body.setMargin(false); // My experience so far, rely on padding, have unbalanced margins wherever margin is introduced..
	body.setPadding(true);
	body.setHeightFull();
	body.setWidthFull();
	body.getStyle().set("outline", "solid 3px");

	_1textFieldRow.setPadding(true);
	_1textFieldRow.getStyle().set("outline", "dashed 3px");
	_1textFieldRow.setAlignItems(Alignment.END);
	_2passwordFieldRow.setPadding(true);
	_2passwordFieldRow.getStyle().set("outline", "dashed 3px");
	_2passwordFieldRow.setAlignItems(Alignment.START);
	_3booleanBoxRow.setPadding(true);
	_3booleanBoxRow.getStyle().set("outline", "dashed 3px");
	_3booleanBoxRow.setAlignItems(Alignment.START);
	_4dateTimePickerRow.setPadding(true);
	_4dateTimePickerRow.getStyle().set("outline", "dashed 3px");
	_4dateTimePickerRow.setAlignItems(Alignment.START);
	_5NumericInput.setPadding(true);
	_5NumericInput.getStyle().set("outline", "dashed 3px");
	_5NumericInput.setAlignItems(Alignment.START);
	_6ComboBoxSingle.setPadding(true);
	_6ComboBoxSingle.getStyle().set("outline", "dashed 3px");
	_6ComboBoxSingle.setAlignItems(Alignment.START);
	_7RadioButtonSingle.setPadding(true);
	_7RadioButtonSingle.getStyle().set("outline", "dashed 3px");
	_7RadioButtonSingle.setAlignItems(Alignment.START);

	body.add(
		_1textFieldRow,
		_2passwordFieldRow,
		_3booleanBoxRow,
		_4dateTimePickerRow,
		_5NumericInput,
		_6ComboBoxSingle,
		_7RadioButtonSingle
	);

	_1textFieldRow.add(
		annotation1,
		textFieldComponent1,
		textFieldComponent2,
		textFieldComponent3
	);
	//endregion

// region PASSWORDFIELD
	passwordFieldComponent1.addBasicReturnFlex();
	passwordFieldComponent1.addWithVerification();


	_2passwordFieldRow.add(
	annotation2,
	passwordFieldComponent1
	);

	// endregion

// region CHECKBOX/BOOLEAN
checkboxComponent1.addBasic();
checkboxComponent2.createBasicCheckbox("Passed label", "Aria label..");


	_3booleanBoxRow.add(
		annotation3,
		checkboxComponent1,
		annotation4,
		checkboxComponent2
	);
	//endregion

// region DATETIME
	// going to nest a Flexbox inside _4dateTimePickerRow and pass it lots of VerticalLayout Date pickers.
	dateTimeFlexRow.setFlexDirection(FlexLayout.FlexDirection.ROW);
	dateTimeFlexRow.setAlignContent(FlexLayout.ContentAlignment.SPACE_AROUND);
	dateTimeFlexRow.getStyle().set("outline", "solid 2px");

	// create instances using my reuseable component code:
	HorizontalLayout holdsDateOnlyPickers = dateTimeInput_Component.getDatePicker();
	HorizontalLayout holdsTimePickers = dateTimeInput_Component2.getTimePicker();
	dateTimeInput_Component.getDatePicker("Well, that's novel!");
	dateTimeInput_Component.getDatePicker(LocalDate.now().minusMonths(3), LocalDate.now().plusMonths(3));
	dateTimeInput_Component2.getDateTimePicker();

	dateTimeFlexRow.add(
		holdsDateOnlyPickers,
		holdsTimePickers

	);

	_4dateTimePickerRow.add(
		annotation5,
		dateTimeFlexRow
	);
	//endregion

// region NUMERIC
	numericFlexRow.setFlexDirection(FlexLayout.FlexDirection.ROW);
	numericFlexRow.setAlignContent(FlexLayout.ContentAlignment.SPACE_AROUND);
	numericFlexRow.getStyle().set("outline", "solid 2px");

	HorizontalLayout holdsNumericExamples = numericInput_Component1.getNumericInput();
	numericInput_Component1.getNumericInput("Another label");
	numericInput_Component1.getIntegerInput("This uses INTEGER FIELD");

	numericFlexRow.add(
		holdsNumericExamples
	);

	_5NumericInput.add(
		annotation6,
		numericFlexRow
	);

	//endregion

// region COMBOBOX SINGLE-SELECT
	comboSingleFlexRow.setFlexDirection(FlexLayout.FlexDirection.ROW);
	comboSingleFlexRow.setAlignContent(FlexLayout.ContentAlignment.SPACE_AROUND);
	comboSingleFlexRow.getStyle().set("outline", "solid 2px");

	singleSelectCombobox1.getComboBox();
	HorizontalLayout wrapperHorizontal = singleSelectCombobox1.wrapComboBoxHorizontal(); // First combo box lives in here

	ComboBox<Department> comboBox2 = singleSelectCombobox2.getComboBox("Dept.(instantiated)");
	comboBox2.setItems(
		new Department(1, "R&D_ts"),
		new Department(2, "Marketing_ts"),
		new Department(3, "Sales_ts")
	);

	mockDepartmentServiceSingleton.activateOrResetList();
	ComboBox<Department> comboBox3 = singleSelectCombobox3.getComboBox("From service, appended", "added info", mockDepartmentServiceSingleton.getDepartmentList());


	wrapperHorizontal.add(
		comboBox2,
		comboBox3
	);
	comboSingleFlexRow.add(
		wrapperHorizontal
	);
	_6ComboBoxSingle.add(
		annotation7,
		comboSingleFlexRow,
		annotation8
	);

	//endregion

	// region RADIO-BUTTON-GROUP SINGLE-SELECT
	radioSingleFlexRow.setFlexDirection(FlexLayout.FlexDirection.ROW);
	radioSingleFlexRow.setAlignContent(FlexLayout.ContentAlignment.SPACE_AROUND);
	radioSingleFlexRow.getStyle().set("outline", "solid 2px");

	singleSelectRadioButtonGroup1.getRadioButtons();
	HorizontalLayout wrapperHorizontal2 = singleSelectRadioButtonGroup1.wrapRadioButtonsHorizontal(); // First combo box lives in here

	RadioButtonGroup<Department> radioButtons2 = singleSelectRadioButtonGroup2.getRadioButtons("with label, instantiated");
	radioButtons2.setItems(
		new Department(1, "R&D_ts"),
		new Department(2, "Marketing_ts"),
		new Department(3, "Sales_ts")
	);

	mockDepartmentServiceSingleton.activateOrResetList();
	RadioButtonGroup<Department> radioButtons3 = singleSelectRadioButtonGroup3.getRadioButtons("List passed from service", mockDepartmentServiceSingleton.getDepartmentList());

	//And a single listBoxExample of LISTBOX
	ListBox<Department> listBoxExample = new ListBox<>();
	listBoxExample.setItems(mockDepartmentServiceSingleton.getDepartmentList());
	listBoxExample.addValueChangeListener(change ->
		                               Notification.show("change.getHasValue(): " + change.getHasValue() + "\n" +
			                               "change.getOldValue().toString(): " + change.getOldValue().toString() + "\n" +
			                                               "change.getValue(): " + change.getValue()));

	// A multiselect example CheckboxGroup example:
	CheckboxGroup<Department> multiselectExample1 = new CheckboxGroup<>();
	multiselectExample1.setItems(mockDepartmentServiceSingleton.getDepartmentList());
	multiselectExample1.addValueChangeListener(change -> {
		Set<Department> valueSet = change.getValue();
		for(Department value: valueSet)
			Notification.show(value.toString());
	});

// A multiselect example CheckboxGroup example:
	MultiSelectListBox<Department> multiselectExample2 = new MultiSelectListBox<>();
	multiselectExample2.setItems(mockDepartmentServiceSingleton.getDepartmentList());
	multiselectExample2.addValueChangeListener(change -> {
		Set<Department> valueSet = change.getValue();
		for(Department value: valueSet)
			Notification.show(value.toString());
	});

	wrapperHorizontal2.add(
		radioButtons2,
		radioButtons3
	);
	radioSingleFlexRow.add(
		wrapperHorizontal2
	);
	_7RadioButtonSingle.add(
		annotation9,
		radioSingleFlexRow,
		annotation10,
		listBoxExample,
		multiselectExample1,
		multiselectExample2

	);
	//endregion

}

}

package com.example.application.pos.models;

// All the UI components and other helper classes in Vaadin implement Serializable.

import com.vaadin.flow.component.Component;
import com.vaadin.flow.component.Composite;
import com.vaadin.flow.component.button.Button;
import com.vaadin.flow.component.checkbox.Checkbox;
import com.vaadin.flow.component.combobox.ComboBox;
import com.vaadin.flow.component.html.H1;
import com.vaadin.flow.component.icon.VaadinIcon;
import com.vaadin.flow.component.orderedlayout.VerticalLayout;
import com.vaadin.flow.component.textfield.TextField;
import com.vaadin.flow.function.SerializableRunnable;

import java.util.Set;

public class ProductForm extends Composite<Component> {

// region SERIALIZEABLE RUNNABLE
/* Allows us to perform a clallback to the clients of this class such as Product ManagementView, Serializable is required for session persistence.
This lambda is called back:
() -> {
		dialog.close();
		save(product);
	}
*/
//endregion

private final SerializableRunnable saveListener;
private Product product;
private TextField name = new TextField("Name");
private ComboBox<Manufacturer> manufacturer = new ComboBox<>();
private Checkbox available = new Checkbox("Available");


// #1: In order to implement the binding in the Product to Input Field direction we set the value of each
// input field from the corresponding Java property:
public ProductForm(Product product, Set<Manufacturer> manufacturers, SerializableRunnable saveListener){
	this.product = product;
	this.saveListener = saveListener;

	manufacturer.setItems(manufacturers);
	manufacturer.setItemLabelGenerator(manufacturer -> manufacturer.getName());
	//manufacturer.setItemLabelGenerator(Manufacturer::getName); // Method reference instead of Lambda

	if (product.getName() != null) {
		name.setValue(product.getName());
		manufacturer.setValue(product.getManufacturer());
		available.setValue(product.isAvailable());
	}

}

@Override
protected Component initContent() {
	return new VerticalLayout(
		new H1("Product"),
		name,
		manufacturer,
		available,
		new Button("Save", VaadinIcon.CHECK.create(),
			event -> saveClicked())
	);
}
/*
#2 Databinding in the opposite direction completed with the below:
 */
private void saveClicked() {
	product.setName(name.getValue());
	product.setManufacturer(manufacturer.getValue());
	product.setAvailable(available.getValue());
	saveListener.run();
}


}

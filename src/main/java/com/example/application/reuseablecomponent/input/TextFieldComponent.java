package com.example.application.reuseablecomponent.input;

import com.vaadin.flow.component.Component;
import com.vaadin.flow.component.Composite;
import com.vaadin.flow.component.notification.Notification;
import com.vaadin.flow.component.orderedlayout.FlexLayout;
import com.vaadin.flow.component.orderedlayout.VerticalLayout;
import com.vaadin.flow.component.textfield.TextField;


public class TextFieldComponent extends Composite<Component> {
FlexLayout flexLayout = new FlexLayout();
TextField verifiable = new TextField(); //there is no need for this see PasswordInputComponent

@Override
protected Component initContent() {
	flexLayout.setFlexDirection(FlexLayout.FlexDirection.ROW);


	return flexLayout;
}

private VerticalLayout getVerticalLayout() {
	VerticalLayout verticalLayout = new VerticalLayout();
	verticalLayout.setPadding(true);
	verticalLayout.getStyle().set("outline", "2px solid");
	return verticalLayout;
}

private TextField createTextField() {
	TextField textField =  new TextField();
	textField.setLabel("AutoSelect/Focus");
	textField.setAutofocus(true);
	textField.setAutoselect(true);
	textField.setClearButtonVisible(true);
	textField.setPlaceholder("With clear button");
	return textField;
}

private TextField returnVerified() {
	verifiable.setLabel("Verification not working set in Composite");
	verifiable.setAutofocus(true);
	verifiable.setAutoselect(true);
	verifiable.setRequired(true);
	verifiable.setMinLength(2);
	verifiable.setMaxLength(10);
	verifiable.setClearButtonVisible(true);
	verifiable.setPlaceholder("between 2 10 characters");
	verifiable.setPattern("^[a-zA-Z\\s]+");
	verifiable.setErrorMessage("2-10 letters only");
	setValueChangeListener();
	return  verifiable;
}

private void setValueChangeListener() {
		verifiable.addValueChangeListener(event -> {
		if(verifiable.isInvalid()) {
			Notification.show("Invalid name");
		}
	});
}

private TextField createBasicTextField() {
	TextField textField =  new TextField();
	textField.setAutofocus(false);
	textField.setAutoselect(false);
	textField.setLabel("BasicTextField");
	textField.setPlaceholder("enter your full name");
	return textField;
}

private void addToFlexLayout(Component passedComponent) {
	this.flexLayout.add(passedComponent);
}

public TextFieldComponent addTextField() {
	VerticalLayout verticalLayout = getVerticalLayout();
	verticalLayout.add(
		createTextField()
	);
	addToFlexLayout(verticalLayout);
	return this;
}

public TextFieldComponent addBasicTextField() {
	VerticalLayout verticalLayout = getVerticalLayout();
	verticalLayout.add(
		createBasicTextField()
	);
	addToFlexLayout(verticalLayout);
	return this;
}

public TextFieldComponent addVerifiedTextField() {
	VerticalLayout verticalLayout = getVerticalLayout();
	verticalLayout.add(
		returnVerified()
	);
	addToFlexLayout(verticalLayout);
	return this;
}




}

package com.example.application.views.layout;

import com.vaadin.flow.component.button.Button;
import com.vaadin.flow.component.orderedlayout.VerticalLayout;
import com.vaadin.flow.router.Route;

@Route("10_Width-Layout")
public class _10WidthView extends VerticalLayout {
Button oneHundred;
Button eighty;
Button three00Pix;
Button rem;
Button tenEm;
Button undefined1;
Button undefined2;




public _10WidthView() {
	add(
		oneHundred = new Button("I'm 100% Wide"),
		eighty = new Button("I'm 80% Wide"),
		three00Pix = new Button("I'm 300px Wide"),
		rem = new Button("1rem Wide"),
		tenEm = new Button("I'm 10em Wide"),
		undefined1= new Button("I'm undefined Wide"),
		undefined2= new Button("Me too")
	);

	oneHundred.setWidth("100%"); // percentage parent size
	eighty.setWidth("80%");
	three00Pix.setWidth("300px");
	rem.setWidth("1rem"); // size of root font
	tenEm.setWidth("10em"); // font size of parent
	undefined1.setSizeUndefined();
	undefined2.setSizeUndefined();
}
}

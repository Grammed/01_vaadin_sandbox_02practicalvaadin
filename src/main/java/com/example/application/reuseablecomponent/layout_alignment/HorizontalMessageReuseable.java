package com.example.application.reuseablecomponent.layout_alignment;

import com.vaadin.flow.component.Component;
import com.vaadin.flow.component.Composite;
import com.vaadin.flow.component.button.Button;
import com.vaadin.flow.component.orderedlayout.FlexComponent;
import com.vaadin.flow.component.orderedlayout.HorizontalLayout;

public class HorizontalMessageReuseable extends Composite<Component>{
HorizontalLayout horizontalLayout = new HorizontalLayout();
private Button messageButton = new Button();

@Override
protected Component initContent() {
	horizontalLayout.add(
		messageButton
	);
	horizontalLayout.setAlignSelf(FlexComponent.Alignment.CENTER, horizontalLayout.getComponentAt(0));
	horizontalLayout.setPadding(true);
	horizontalLayout.setMargin(false);
	horizontalLayout.setHeight("5rem");
	horizontalLayout.getStyle().set("border", "5px solid");
	return horizontalLayout;
}

public HorizontalMessageReuseable setMessageReuseableText(String message) {
	this.messageButton.setText(message);
	return this;
}
}
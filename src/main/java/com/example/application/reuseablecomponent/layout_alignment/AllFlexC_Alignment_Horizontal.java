package com.example.application.reuseablecomponent.layout_alignment;

import com.vaadin.flow.component.Component;
import com.vaadin.flow.component.Composite;
import com.vaadin.flow.component.button.Button;
import com.vaadin.flow.component.orderedlayout.FlexComponent;
import com.vaadin.flow.component.orderedlayout.HorizontalLayout;

public class AllFlexC_Alignment_Horizontal extends Composite<Component> {
HorizontalLayout horizontalLayout = new HorizontalLayout();

@Override
protected Component initContent() {
	horizontalLayout.add(
		new Button("START 1"),
		new Button("CENTER 2"),
		new Button("END 3"),
		new Button("STRETCH 4"),
		new Button("BASELINE 5")
	);
	horizontalLayout.setAlignSelf(FlexComponent.Alignment.START, horizontalLayout.getComponentAt(0));
	horizontalLayout.setAlignSelf(FlexComponent.Alignment.CENTER, horizontalLayout.getComponentAt(1));
	horizontalLayout.setAlignSelf(FlexComponent.Alignment.END, horizontalLayout.getComponentAt(2));
	horizontalLayout.setAlignSelf(FlexComponent.Alignment.STRETCH, horizontalLayout.getComponentAt(3));
	horizontalLayout.setAlignSelf(FlexComponent.Alignment.BASELINE, horizontalLayout.getComponentAt(4));
	horizontalLayout.setMargin(false);
	horizontalLayout.setPadding(true);
	horizontalLayout.setHeight("15rem");
	horizontalLayout.getStyle().set("border", "1px solid");
	return horizontalLayout;
}


}

package com.example.application.reuseablecomponent.layout_alignment;

import com.vaadin.flow.component.Component;
import com.vaadin.flow.component.Composite;
import com.vaadin.flow.component.html.Div;
import com.vaadin.flow.component.orderedlayout.FlexLayout;
import com.vaadin.flow.component.orderedlayout.HorizontalLayout;

public class Annotation extends Composite<Component> {
	HorizontalLayout padder = new HorizontalLayout();
	FlexLayout flexLayout = new FlexLayout();


	@Override
protected Component initContent() {
		padder.setPadding(true);
		padder.add(flexLayout);
		padder.getStyle().set("outline", "dashed 1px");

		flexLayout.setFlexDirection(FlexLayout.FlexDirection.COLUMN);

return padder;
}

public Annotation createAndSetText(String text) {
	Div div = new Div();
	div.setText(text);
	this.flexLayout.add(div);
	return this;
}



}

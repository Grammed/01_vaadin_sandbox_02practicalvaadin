package com.example.application.views.component._02upload;


import com.example.application.reuseablecomponent.Upload_1;
import com.vaadin.flow.component.orderedlayout.FlexComponent;
import com.vaadin.flow.component.orderedlayout.FlexLayout;
import com.vaadin.flow.component.orderedlayout.HorizontalLayout;
import com.vaadin.flow.component.orderedlayout.VerticalLayout;
import com.vaadin.flow.router.Route;

@Route("02_Upload-Components")
public class _02Upload_View extends VerticalLayout {
VerticalLayout body = new VerticalLayout();
FlexLayout flexRow1 = new FlexLayout();
Upload_1 singleInMemory = new Upload_1();
Upload_1 multiinMemory = new Upload_1();
Upload_1 singleTempFile = new Upload_1();
Upload_1 multiTempFile = new Upload_1();


public _02Upload_View() {
	this.add(
		body
	);

	body.setMargin(false);// Setting margin here throws border off page
	body.setPadding(true);
	body.setHeightFull();
	body.setWidthFull();
	body.getStyle().set("outline", "solid 10px");
	body.setAlignItems(FlexComponent.Alignment.CENTER);

	flexRow1.setFlexDirection(FlexLayout.FlexDirection.ROW);
	flexRow1.getStyle().set("outline", "dashed 10px");


	body.add(
		flexRow1
	);

	// region SINGLE IN MEMORY UPLOAD
	singleInMemory.getUploadSingleFileInMemory();
	HorizontalLayout wrappedHorizontal = singleInMemory.wrapHorizontal();
	multiinMemory.getUploadMultiFileInMemory();
	singleInMemory.addOnSuccessBusinessLogic_01();









	VerticalLayout wrappedVertical = multiinMemory.wrapVertical();
	singleTempFile.getUploadSingleFileCreate();
	HorizontalLayout wrappedHorizontal1 = singleTempFile.wrapHorizontal();
	multiTempFile.getUploadMultiFileCreate();
	VerticalLayout wrappedVertical1 = multiTempFile.wrapVertical();

	flexRow1.add(
		// unwrapped version
		//singleInMemory,
		//multiinMemory,
		//singleTempFile,
		//multiTempFile

		wrappedHorizontal,
		wrappedVertical,
		wrappedHorizontal1,
		wrappedVertical1
	);




}
}

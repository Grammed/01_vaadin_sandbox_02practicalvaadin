package com.example.application.reuseablecomponent.visualisation;

import com.vaadin.flow.component.Component;
import com.vaadin.flow.component.Composite;
import com.vaadin.flow.component.Text;
import com.vaadin.flow.component.button.Button;
import com.vaadin.flow.component.notification.Notification;
import com.vaadin.flow.component.orderedlayout.VerticalLayout;
import static com.vaadin.flow.component.notification.Notification.Position;

public class BuiltNotification extends Composite<Component> {
	Component component;
	private boolean decisionBoolean;
	private Button mainButton;
	private int count;

	protected Component initContent() {
		return component;
	}

	public VerticalLayout getNotificationButtonVerticalWrap() {
		VerticalLayout notificationButtonWrapper = new VerticalLayout();
		decisionBoolean = true;
		notificationButtonWrapper.add(
		getStandardNotificationButton()
		);
		return notificationButtonWrapper;

	}

	public VerticalLayout getNotificationButtonVerticalWrap(String buttonName, String notificationText, Position position, int duration) {
		decisionBoolean = true;
		VerticalLayout notificationButtonWrapper = new VerticalLayout();
		mainButton = new Button(buttonName);
		setNotificationEvent(notificationText, position, duration);
		notificationButtonWrapper.add(
			mainButton
		);
		return notificationButtonWrapper;
	}

private void setNotificationEvent(String notificationText, Position position, int duration) {
		mainButton.addClickListener(clickEvent -> {
			Notification notification = new Notification(new VerticalLayout(new Text(notificationText)));
			notification.setPosition(position);
			if(decisionBoolean) {
				notification.add(new Button("Close", e ->
					                                     notification.close()));
			} else {
				notification.setDuration(duration);
			}
			decisionBoolean = false;
			notification.open();
		});
}


private Button getStandardNotificationButton() {
		return new Button("Standard Notification", event -> {
			Notification notification = new Notification();
			notification.add(new VerticalLayout(
				new Text("Text set in getStandardNotificationButton()!")));
			notification.setPosition(Notification.Position.MIDDLE);
			if (decisionBoolean) {
				//notification.setDuration(0);
				notification.add(new Button("Close", e ->
					                                     notification.close()));
			} else {
				notification.setDuration(2000);
			}
			decisionBoolean = false;
			notification.open();
		});
	}



}

package com.example.application.views.layout;

import com.example.application.reuseablecomponent.layout_alignment._01Scroller;
import com.vaadin.flow.component.orderedlayout.VerticalLayout;
import com.vaadin.flow.router.Route;

@Route("04_Scrolling-Layout")
public class _04ScrollingView extends VerticalLayout {
VerticalLayout body = new VerticalLayout();
_01Scroller scrollers = new _01Scroller();



public _04ScrollingView() {
	body.setMargin(false); // My experience so far, rely on padding, have unbalanced margins wherever margin is introduced..
	body.setPadding(true);
	body.setHeightFull();
	body.setWidthFull();
	body.add(
		scrollers
	);

	add(
		body
	);

}


}

package com.example.application.reuseablecomponent.visualisation;

import com.vaadin.flow.component.Component;
import com.vaadin.flow.component.Composite;
import com.vaadin.flow.component.Text;
import com.vaadin.flow.component.button.Button;
import com.vaadin.flow.component.dialog.Dialog;
import com.vaadin.flow.component.orderedlayout.FlexComponent;
import com.vaadin.flow.component.orderedlayout.VerticalLayout;


public class BuiltPopUpDialog extends Composite<Component> {
	Component component;

	protected Component initContent() {
		return component;
	}

	public VerticalLayout getPopUpButton() {
		VerticalLayout popUpButtonWrapper = new VerticalLayout();
		popUpButtonWrapper.add(
		getStandardPopUpButton()
		);
		return popUpButtonWrapper;

	}


private Button getStandardPopUpButton() {


		return new Button("Draggable PopUpDialog", event -> {
			VerticalLayout verticalLayout = new VerticalLayout();
			verticalLayout.setAlignItems(FlexComponent.Alignment.CENTER);
			verticalLayout.add(new Text("Will only close with button press"));Dialog dialog = new Dialog();

			Button closeButton = new Button("Close", e -> dialog.close());
			//closeButton.setWidthFull();
			verticalLayout.add(
			closeButton
			);
			dialog.add(
				verticalLayout
				);
			dialog.setResizable(true);
			dialog.setDraggable(true);
			dialog.setCloseOnEsc(false);
			dialog.setCloseOnOutsideClick(false);
			dialog.setModal(true); // no interaction with rest of app
			dialog.open();
		});
	}



}

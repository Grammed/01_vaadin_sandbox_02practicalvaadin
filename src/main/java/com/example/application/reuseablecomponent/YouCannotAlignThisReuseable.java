package com.example.application.reuseablecomponent;

import com.vaadin.flow.component.Component;
import com.vaadin.flow.component.Composite;
import com.vaadin.flow.component.button.Button;
import com.vaadin.flow.component.orderedlayout.VerticalLayout;

public class YouCannotAlignThisReuseable extends Composite {
private Button button1 = new Button("V_Reuseable 1");
private Button button2 = new Button("V_Reuseable 2");
private Button button3 = new Button("V_Reuseable 3");

protected Component initContent() {

	return new VerticalLayout(
		button1,
		button2,
		button3
	);
}

public Button getButton1() {
	return button1;
}

public Button getButton2() {
	return button2;
}

public Button getButton3() {
	return button3;
}
}


package com.example.application.views.component._12interaction;


import com.example.application.reuseablecomponent.interaction.BuiltLink;
import com.example.application.reuseablecomponent.interaction.BuiltMenuBar;
import com.example.application.reuseablecomponent.interaction.TimeButton;
import com.example.application.reuseablecomponent.layout_alignment.Annotation;
import com.vaadin.flow.component.button.Button;
import com.vaadin.flow.component.html.Anchor;
import com.vaadin.flow.component.menubar.MenuBar;
import com.vaadin.flow.component.orderedlayout.FlexLayout;
import com.vaadin.flow.component.orderedlayout.HorizontalLayout;
import com.vaadin.flow.component.orderedlayout.VerticalLayout;
import com.vaadin.flow.router.Route;
import static com.example.application.tools.CentralBasicStyling.*;

@Route("02a_Interaction_Components_12")
public class _12InteractionView extends VerticalLayout {
VerticalLayout body;
//FlexLayout body;
HorizontalLayout horizontal1ButtonRow;
FlexLayout flex2AnchorRow;
HorizontalLayout horizontal3MenuRow;
Annotation flex1ButtonRowAnnotation = new Annotation().createAndSetText("Interactive BUTTONS...");
VerticalLayout timeButton1Wrap;
TimeButton timeButton1;
Button button1;
VerticalLayout timeButton2Wrap;
Button button2;
VerticalLayout timeButton3Wrap;
Button button3;
Annotation flex2AnchorRowAnnotation = new Annotation().createAndSetText("ANCHOR LINKS with and without BUTTONS, the last creates and downloads a file using 'StreamResource'...");
VerticalLayout builtLink1Wrap;
BuiltLink builtLink1;
Anchor anchor1;
VerticalLayout builtLink2Wrap;
Anchor anchor2;
VerticalLayout builtLink3Wrap;
Anchor anchor3;
Annotation flex3MenuBarRowAnnotation = new Annotation().createAndSetText("Menu hierarchies: 1 -> no action 2 -> click event added via list 3 -> context with click events added by same method." +
	                                                                         " FIRST EXAMPLE of Composite<Component> actually returning different components.");
VerticalLayout builtMenu1Wrap;
BuiltMenuBar builtMenuBar1;
MenuBar menuBar1;
VerticalLayout builtMenu2Wrap;
MenuBar menuBar2;
VerticalLayout builtMenu3Wrap;
HorizontalLayout contextMenu3;



public _12InteractionView() {
	this.body = createDefaultVerticalBody();
	this.horizontal1ButtonRow = createDefaultHorizontalBodyRow();
	this.flex2AnchorRow = createDefaultFlexBodyRow();
	this.horizontal3MenuRow = createDefaultHorizontalBodyRow();

	this.add(
		body
	);

	body.add(
		horizontal1ButtonRow,
		flex2AnchorRow,
		horizontal3MenuRow
	);

	// region BUTTON EXAMPLES
	this.timeButton1 = new TimeButton();
	button1 = timeButton1.getBasicTimeButton();
	button2 = timeButton1.getOneShotTimeButton();
	button3 = timeButton1.getOneShotDisappearTimeButton();
	timeButton1Wrap = createNonFlexVerticalWrapper();
	timeButton2Wrap = createNonFlexVerticalWrapper();
	timeButton3Wrap = createNonFlexVerticalWrapper();

	timeButton1Wrap.add(
		button1
	);

	timeButton2Wrap.add(
		button2
	);

	timeButton3Wrap.add(
		button3
	);

	horizontal1ButtonRow.add(
		flex1ButtonRowAnnotation,
		timeButton1Wrap,
		timeButton2Wrap,
		timeButton3Wrap
	);
// endregion

// region LINK EXAMPLES
	this.builtLink1 = new BuiltLink();
	anchor1 = builtLink1.getBasicBlogPlugLink();
	anchor1.setId("anchor1");
	anchor2 = builtLink1.getButtonBlogPlugLink();
	anchor3 = builtLink1.getTextLinkGeneratedAtRuntime();
	builtLink1Wrap = createNonFlexVerticalWrapper();
	builtLink2Wrap = createNonFlexVerticalWrapper();
	builtLink3Wrap = createNonFlexVerticalWrapper();


	builtLink1Wrap.add(
		anchor1
	);

	builtLink2Wrap.add(
		anchor2
	);

	builtLink3Wrap.add(
		anchor3
	);

	flex2AnchorRow.add(
		flex2AnchorRowAnnotation,
		builtLink1Wrap,
		builtLink2Wrap,
		builtLink3Wrap
	);


	// region RETRIEVE WRAPPED COMPONENT

// todo with getChildren()?? retrieve wrapped component
	// want to do this so composite templates can have their internals edited

	// endregion


	//endregion

	// region MENUBAR EXAMPLES

	this.builtMenuBar1 = new BuiltMenuBar();
	menuBar1 = builtMenuBar1.getBasicFileEdit("menubar1-interaction-view");
	menuBar2 = builtMenuBar1.getActionFileEdit("menubar2-interaction-view");
	contextMenu3 = builtMenuBar1.getContextMenuHorizontalWrapped("contextholder-interaction-view", "contextbar-interaction-view", "Right click here");
	builtMenu1Wrap = createNonFlexVerticalWrapper();
	builtMenu2Wrap = createNonFlexVerticalWrapper();
	builtMenu3Wrap = createNonFlexVerticalWrapper();

	builtMenu1Wrap.add(
		menuBar1
	);

	builtMenu2Wrap.add(
		menuBar2
	);

	builtMenu3Wrap.add(
		contextMenu3
	);

	horizontal3MenuRow.add(
		flex3MenuBarRowAnnotation,
		builtMenu1Wrap,
		builtMenu2Wrap,
		builtMenu3Wrap
	);

	//endregion

}


}

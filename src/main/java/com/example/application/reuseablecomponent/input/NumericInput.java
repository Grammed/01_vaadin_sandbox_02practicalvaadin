package com.example.application.reuseablecomponent.input;

import com.vaadin.flow.component.Component;
import com.vaadin.flow.component.Composite;
import com.vaadin.flow.component.notification.Notification;
import com.vaadin.flow.component.orderedlayout.HorizontalLayout;
import com.vaadin.flow.component.textfield.IntegerField;
import com.vaadin.flow.component.textfield.NumberField;


public class NumericInput extends Composite<Component> {
	HorizontalLayout numericInputHorizontalHolder = new HorizontalLayout();

@Override
protected Component initContent() {

	return numericInputHorizontalHolder;
}

public HorizontalLayout getNumericInput() {
	NumberField numberField = new NumberField("Rating");
	numberField.setHasControls(true);
	numberField.setMin(0.0);
	numberField.setMax(5.0);
	numberField.setStep(0.5);
	numberField.setClearButtonVisible(true);
	numberField.setHelperText("From 0.0 to 5.0");
	numberField.addValueChangeListener(event -> {
		Double value = event.getValue();
		Notification.show("Your rating: " + value);
	});

	numericInputHorizontalHolder.add(
		numberField
	);
	setDefaultStyle();
	return numericInputHorizontalHolder;
}

public HorizontalLayout getNumericInput(String label) {
	NumberField numberField = new NumberField(label);
	numberField.setHasControls(true);
	numberField.setMin(0.0);
	numberField.setMax(5.0);
	numberField.setStep(0.5);
	numberField.setClearButtonVisible(true);
	numberField.setHelperText("From 0.0 to 5.0");
	numberField.addValueChangeListener(event -> {
		Double value = event.getValue();
		Notification.show("Your rating: " + value);
	});

	numericInputHorizontalHolder.add(
		numberField
	);
	setDefaultStyle();
	return numericInputHorizontalHolder;
}


public HorizontalLayout getIntegerInput(String label) {
	IntegerField integerField = new IntegerField(label);
	integerField.setHasControls(true);
	integerField.setMin(1);
	integerField.setMax(10);
	integerField.setStep(1);
	integerField.setClearButtonVisible(true);
	integerField.setHelperText("Set score between 1 and 10");
	integerField.addValueChangeListener(event -> {
		Integer value = event.getValue();
		Notification.show("Your rating: " + value);
	});

	numericInputHorizontalHolder.add(
	integerField
	);
	setDefaultStyle();
	return numericInputHorizontalHolder;
}


private void setDefaultStyle() {
	this.numericInputHorizontalHolder.getStyle().set("outline", "4px dashed");
	this.numericInputHorizontalHolder.setPadding(true);
	this.numericInputHorizontalHolder.setMargin(true);
}

}

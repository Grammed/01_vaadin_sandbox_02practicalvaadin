package com.example.application.views.layout;

import com.example.application.reuseablecomponent.layout_alignment.HorizontalMessageReuseable;
import com.vaadin.flow.component.button.Button;
import com.vaadin.flow.component.orderedlayout.FlexLayout;
import com.vaadin.flow.component.orderedlayout.HorizontalLayout;
import com.vaadin.flow.component.orderedlayout.VerticalLayout;
import com.vaadin.flow.router.Route;

@Route("06_Grow-Shrink-Layout")
public class _06GrowView extends VerticalLayout {
Button button1a = new Button();
Button button2a = new Button();
HorizontalLayout hLay1 = new HorizontalLayout(button1a, button2a);
Button button1b = new Button();
Button button2b = new Button();
HorizontalLayout hLay2 = new HorizontalLayout(button1b, button2b);
Button button1c = new Button();
Button button2c = new Button();
HorizontalLayout hLay3 = new HorizontalLayout(button1c, button2c);
Button button3 = new Button();
Button button4 = new Button();
HorizontalMessageReuseable message = new HorizontalMessageReuseable().setMessageReuseableText("FlexShrink doesn't show up!");
FlexLayout hLay4 = new FlexLayout(button3, button4);
VerticalLayout vLayMain = new VerticalLayout(hLay1, hLay2, hLay3, message, hLay4);


public _06GrowView() {
		add(
			vLayMain
		);

	vLayMain.setMargin(false);
	vLayMain.setPadding(true);

	button3.setWidth("15rem");
	button4.setWidth("10rem");
	hLay4.setWidth("20rem");
	hLay4.getStyle().set("border", "dashed 1px");
	hLay4.setFlexGrow(8d, button3);
	hLay4.setFlexShrink(2d, button4);
	button3.setText("(8d, button3");
	button4.setText("(2d, button4");

	hLay1.setWidthFull();
	hLay1.getStyle().set("border", "1px solid");
	hLay1.setMargin(false);
	hLay1.setPadding(true);
	hLay1.setFlexGrow(0, button1a);
	hLay1.setFlexGrow(0, button2a);
	button1a.setText("flexGrow0");
	button2a.setText("flexGrow0");

	hLay2.setWidthFull();
	hLay2.getStyle().set("border", "1px solid");
	hLay2.setMargin(false);
	hLay2.setPadding(true);
	hLay2.setFlexGrow(3, button1b);
	hLay2.setFlexGrow(7, button2b);
	button1b.setText("flexGrow3");
	button2b.setText("flexGrow7");

	hLay3.setWidthFull();
	hLay3.getStyle().set("border", "1px solid");
	hLay3.setMargin(false);
	hLay3.setPadding(true);
	hLay3.setSpacing(true);
	hLay3.setFlexGrow(10, button1c);
	hLay3.setFlexGrow(10, button2c);
	button1c.setText("flexGrow10");
	button2c.setText("flexGrow10");
	}
}
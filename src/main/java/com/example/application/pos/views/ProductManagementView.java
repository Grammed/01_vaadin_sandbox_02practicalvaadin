package com.example.application.pos.views;

import com.example.application.pos.models.Manufacturer;
import com.example.application.pos.models.Product;
import com.example.application.pos.models.ProductForm;
import com.vaadin.flow.component.Component;
import com.vaadin.flow.component.Composite;
import com.vaadin.flow.component.button.Button;
import com.vaadin.flow.component.details.Details;
import com.vaadin.flow.component.dialog.Dialog;
import com.vaadin.flow.component.icon.VaadinIcon;
import com.vaadin.flow.component.notification.Notification;
import com.vaadin.flow.component.orderedlayout.HorizontalLayout;
import com.vaadin.flow.component.orderedlayout.VerticalLayout;
import com.vaadin.flow.router.Route;

import java.util.HashSet;
import java.util.Set;

@Route("00_POS_ProductManagementView")
public class ProductManagementView extends Composite<Component> {
private static Set<Product> products = new HashSet<>();
private static Set<String> strings = new HashSet<>();
private static Set<Manufacturer> manufacturers = new HashSet<>();

static {
	manufacturers.add(new Manufacturer("Unique Beers", "0283402", "beerxample@address.com"));
	manufacturers.add(new Manufacturer("Formaldehyde Beers", "0438208", "FormalHide@ingredients.com"));
	manufacturers.add(new Manufacturer("Enemy now Beers", "020830823", "TranslatedFrom@chinese.com"));
	manufacturers.add(new Manufacturer("Some Drinks Liquid Company", "028040208", "wetliquid@address.com"));
}

private VerticalLayout productsLayout = new VerticalLayout();

@Override
protected Component initContent() {
	updateList();
	return new VerticalLayout(
		new Button("New product", VaadinIcon.PLUS.create(),
			clickHeard -> showProductForm(new Product())),
		productsLayout
	);
}

private void updateList() {
	productsLayout.removeAll();
	products.stream()
		.map(product -> new Details (
			product.getName() +
				((product.isAvailable()) ? "" : " (not available"),
			new HorizontalLayout(
				new Button(VaadinIcon.PENCIL.create(),
					clickHeard -> showProductForm(product)),
				new Button(VaadinIcon.TRASH.create(),
					clickHeard -> delete(product))
			)
		))
		.forEach(product -> productsLayout.add(product));
		//.forEach(productsLayout::add);
}

private void showProductForm(Product product) {
	Dialog dialog = new Dialog();
	dialog.setModal(true);
	dialog.open();

	dialog.add(
		//The third argument below is a callback function that will close dialog and save product.
	new ProductForm(product, manufacturers, () -> {
		dialog.close();
		save(product);
	})
	);
}

private void save(Product product) {
	products.add(product);
	updateList();
	Notification.show("Product saved " + product.getName());
}

private void delete(Product product) {
	products.remove(product);
	updateList();
	Notification.show(product.getName() + " was deleted");
}

}

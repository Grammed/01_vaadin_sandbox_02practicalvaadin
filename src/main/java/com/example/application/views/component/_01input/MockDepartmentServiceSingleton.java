package com.example.application.views.component._01input;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import static java.util.Objects.isNull;

// A mock service singleton that returns the departmentList in its current state.
public class MockDepartmentServiceSingleton {
private  List<Department> departmentList = new ArrayList<>();
private  static MockDepartmentServiceSingleton instance = new MockDepartmentServiceSingleton();

private MockDepartmentServiceSingleton() {}


public static MockDepartmentServiceSingleton getInstance() {return instance;}

public int get (Department dept) {
		if(departmentList.contains(dept))
	return departmentList.indexOf(dept);
		else return -1;
}

public List<Department> getDepartmentList(){
	return this.departmentList;
}

public Collection<String> getListDepartmentNames(){
	if(departmentList.isEmpty())
		throw new IllegalStateException("The department list is empty/has not been activated");
	else {
		Collection<String> departmentNameList = new ArrayList<>();
		for (Department dept : departmentList)
			departmentNameList.add(dept.toString());
		return departmentNameList;
	}
}

public void set(Department dept) {
	if(!isNull(dept)) {
		departmentList.add(dept);
	}
}

public void activateOrResetList() {
	departmentList.clear();
	departmentList.add(new Department(1, "R&D_l"));
	departmentList.add(new Department(2, "Marketing_l"));
	departmentList.add(new Department(3, "Sales_l"));
	departmentList.add(new Department(4, "Finance_l"));
}

}

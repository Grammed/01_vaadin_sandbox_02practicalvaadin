package com.example.application.views.layout;

import com.vaadin.flow.component.button.Button;
import com.vaadin.flow.component.html.Paragraph;
import com.vaadin.flow.component.orderedlayout.HorizontalLayout;
import com.vaadin.flow.component.orderedlayout.VerticalLayout;
import com.vaadin.flow.router.Route;

@Route("07_Composition-Layout")
public class _07CompositionView extends VerticalLayout {

public _07CompositionView() {
	var toolbar = new HorizontalLayout(
		new Button("Horizontal Button 1"),
		new Button("Horizontal Button 2"),
		new Button("Horizontal Button 3")
	);
	add(
		toolbar,
		new Paragraph("Paragraph 1 in class's vertical layout"),
		new Paragraph("Paragraph 2 in class's vertical layout"),
		new Paragraph("Paragraph 3 in class's vertical layout")
	);
}
}

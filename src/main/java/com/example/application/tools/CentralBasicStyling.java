package com.example.application.tools;

import com.vaadin.flow.component.html.Image;
import com.vaadin.flow.component.orderedlayout.FlexComponent;
import com.vaadin.flow.component.orderedlayout.FlexLayout;
import com.vaadin.flow.component.orderedlayout.HorizontalLayout;
import com.vaadin.flow.component.orderedlayout.VerticalLayout;

public final class CentralBasicStyling {

// region VIEW BODY

// View body is required, I tried doing with 'this directly' border was lost.

public static VerticalLayout createDefaultVerticalBody() {
	VerticalLayout verticalLayout = new VerticalLayout();
	verticalLayout.setClassName("default-vertical-body");
	verticalLayout.setMargin(false);// Setting margin here throws border off page
	verticalLayout.setPadding(true);
	verticalLayout.setHeightFull();
	verticalLayout.setWidthFull();
	verticalLayout.getStyle().set("outline", "solid 5px");
	verticalLayout.setAlignItems(FlexComponent.Alignment.CENTER);
	return verticalLayout;
}

public static FlexLayout createDefaultFlexVerticalBody() {
	FlexLayout flexLayout = new FlexLayout();
	flexLayout.setFlexDirection(FlexLayout.FlexDirection.COLUMN);
	flexLayout.setClassName("default-flex-vertical-body");
	flexLayout.setHeightFull();
	flexLayout.setWidthFull();
	flexLayout.getStyle().set("outline", "solid 10px");
	flexLayout.setAlignItems(FlexComponent.Alignment.CENTER);
	flexLayout.setAlignContent(FlexLayout.ContentAlignment.SPACE_BETWEEN);
	return flexLayout;
}

// endregion

// region BODY ROW
public static HorizontalLayout createDefaultHorizontalBodyRow() {
	HorizontalLayout horizontalLayout = new HorizontalLayout();
	horizontalLayout.setWidthFull();
	horizontalLayout.setClassName("default-horizontal-row");
	horizontalLayout.setPadding(true);
	horizontalLayout.getStyle().set("outline", "dashed 5px");
	return horizontalLayout;
}

public static FlexLayout createDefaultFlexBodyRow() {
	FlexLayout flexLayout = new FlexLayout();
	flexLayout.setClassName("default-flex-body-row");
	flexLayout.setFlexDirection(FlexLayout.FlexDirection.ROW);
	flexLayout.setAlignContent(FlexLayout.ContentAlignment.SPACE_BETWEEN);
	flexLayout.getStyle().set("outline", "dashed 10px");
	return flexLayout;
}

//endregion

// region WRAP COMPONENT HORIZONTAL
public static HorizontalLayout createNonFlexHorizontalWrapper() {
	HorizontalLayout horizontalLayout = new HorizontalLayout();
	horizontalLayout.setClassName("component-wrapped-horizontal");
	horizontalLayout.setPadding(true);
	horizontalLayout.setMargin(true);
	horizontalLayout.setDefaultVerticalComponentAlignment(FlexComponent.Alignment.CENTER);
	horizontalLayout.getStyle().set("outline", "dashed 7px");
	return horizontalLayout;
}

// endregion

// region WRAP COMPONENT VERTICAL
public static VerticalLayout createNonFlexVerticalWrapper() {
	VerticalLayout verticalLayout = new VerticalLayout();
	verticalLayout.setClassName("component-wrapped-vertical");
	verticalLayout.setPadding(true);
	verticalLayout.setMargin(true);
	verticalLayout.setDefaultHorizontalComponentAlignment(FlexComponent.Alignment.CENTER);
	verticalLayout.getStyle().set("outline", "dashed 7px");
	return verticalLayout;
}

// endregion

// region IMAGES
public static Image createRandomSquare900pxImage() {
	return new Image("https://source.unsplash.com/random/900x900",
		"A random image, think different, think random..");
}


// endregion


}

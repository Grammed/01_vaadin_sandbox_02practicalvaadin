package com.example.application.views.component._13visualisation;

import com.example.application.reuseablecomponent.layout_alignment.HorizontalMessageReuseable;
import com.example.application.reuseablecomponent.visualisation.BuiltNotification;
import com.example.application.reuseablecomponent.visualisation.BuiltPopUpDialog;
import com.vaadin.flow.component.button.Button;
import com.vaadin.flow.component.html.Image;
import com.vaadin.flow.component.icon.Icon;
import com.vaadin.flow.component.icon.VaadinIcon;
import com.vaadin.flow.component.orderedlayout.HorizontalLayout;
import com.vaadin.flow.component.orderedlayout.VerticalLayout;
import com.vaadin.flow.component.tabs.Tab;
import com.vaadin.flow.router.Route;
import org.vaadin.tabs.PagedTabs;

import static com.example.application.tools.CentralBasicStyling.*;
import static com.vaadin.flow.component.notification.Notification.Position;

@Route("02b_Visualisation_Components_13")
public class _13VisualisationView extends VerticalLayout {
VerticalLayout body;
HorizontalLayout horizontal1NotificationRow;
BuiltNotification builtNotification1;
BuiltNotification builtNotification2;
VerticalLayout standardNotification1;
VerticalLayout notificationVertical1;

HorizontalLayout horizontalPopUpDialogRow;
BuiltPopUpDialog builtPopUpDialog1;
VerticalLayout popUpDialogVertical1;

HorizontalLayout quickTabAddOnDemoRow;

HorizontalLayout iconUseRow;
HorizontalMessageReuseable iconRowMessage;

HorizontalLayout nonFlexHorizontalPhotoRow1;
HorizontalLayout nonFlexHorizontalPhotoRow2;
HorizontalLayout nonFlexHorizontalPhotoRow3;


public _13VisualisationView() {
	this.body = createDefaultVerticalBody();
	// The rows in this body:
	this.horizontal1NotificationRow = createDefaultHorizontalBodyRow();
	this.horizontalPopUpDialogRow = createDefaultHorizontalBodyRow();
	this.quickTabAddOnDemoRow = createDefaultHorizontalBodyRow();
	this.iconUseRow = createDefaultHorizontalBodyRow();
	this.nonFlexHorizontalPhotoRow1 = createNonFlexHorizontalWrapper();
	this.nonFlexHorizontalPhotoRow2 = createNonFlexHorizontalWrapper();
	this.nonFlexHorizontalPhotoRow3 = createNonFlexHorizontalWrapper();

	this.iconRowMessage = new HorizontalMessageReuseable().setMessageReuseableText("ICON USE ROW");



	this.builtNotification1 = new BuiltNotification();
	this.builtNotification2 = new BuiltNotification();
	this.builtPopUpDialog1 = new BuiltPopUpDialog();


	this.add(
		body
	);

	body.add(
		horizontal1NotificationRow,
		horizontalPopUpDialogRow,
		quickTabAddOnDemoRow,
		iconUseRow,
		nonFlexHorizontalPhotoRow1,
		nonFlexHorizontalPhotoRow2,
		nonFlexHorizontalPhotoRow3

	);

	standardNotification1 = builtNotification1.getNotificationButtonVerticalWrap();
	notificationVertical1 =builtNotification2.getNotificationButtonVerticalWrap("Notification", "This reuseable notification now plugs directly into static Enum\ncomponent.notification.Notification.Position ", Position.TOP_STRETCH , 4000);


	horizontal1NotificationRow.add(
		standardNotification1,
		notificationVertical1
	);

	popUpDialogVertical1 = builtPopUpDialog1.getPopUpButton();

	horizontalPopUpDialogRow.add(
	popUpDialogVertical1
	);

/* was going to todo Make reuseable of this but.. cannot remove/change component at present.
*/
	HorizontalLayout horizontalLayout = new HorizontalLayout();
	PagedTabs tabs = new PagedTabs(horizontalLayout);
	HorizontalMessageReuseable component1 = new HorizontalMessageReuseable();
	HorizontalMessageReuseable component2 = new HorizontalMessageReuseable();
	HorizontalMessageReuseable component3 = new HorizontalMessageReuseable();
	component1.setMessageReuseableText("Component 1");
	component2.setMessageReuseableText("Component 2");
	component3.setMessageReuseableText("Component 3");
	tabs.add("ComponentOne", component1);
	tabs.add("ComponentTwo", component2);
	tabs.add("ComponentThree", component3);
	Tab tab2 = tabs.get("ComponentTwo");
	tab2.setLabel("Figaro");


	quickTabAddOnDemoRow.add(
	tabs, horizontalLayout
	);

	//ICONS
	Icon bareIcon = VaadinIcon.YOUTUBE_SQUARE.create();
	Icon bareIcon1 = VaadinIcon.CAR.create();


	HorizontalLayout hContainer = new HorizontalLayout();
	HorizontalLayout hContainer1 = new HorizontalLayout();

	Icon hWrapped = VaadinIcon.ARROWS.create();
	Icon hWrapped1 = VaadinIcon.ABACUS.create();
	hContainer.add( hWrapped);
	hContainer1.add(hWrapped1);

	Button button = new Button("Create", VaadinIcon.EDIT.create());



	bareIcon.setSize("4em");
	bareIcon1.setSize("4em");
	hWrapped.setSize("4em");
	hWrapped1.setSize("4em");

	iconUseRow.add(
		iconRowMessage,
		bareIcon,
		bareIcon1,
		hContainer,
		hContainer1,
		button
	);

	// PHOTO ROW

	/*
	* https://source.unsplash.com/random/900x900
	*
	* */

	nonFlexHorizontalPhotoRow1.setWidthFull();

	VerticalLayout photo1Wrapper = createNonFlexVerticalWrapper();
	VerticalLayout photo2Wrapper = createNonFlexVerticalWrapper();
	VerticalLayout photo3Wrapper = createNonFlexVerticalWrapper();

	Image photo1;
	photo1Wrapper.add(
	photo1 = createRandomSquare900pxImage());

	Image photo2;
	photo2Wrapper.add(
		photo2 = createRandomSquare900pxImage());

	Image photo3;
	photo3Wrapper.add(
		photo3 = createRandomSquare900pxImage());


	photo1.setWidthFull();
	photo2.setWidthFull();
	photo3.setWidthFull();

	nonFlexHorizontalPhotoRow1.add(
		photo1Wrapper,
		photo2Wrapper,
		photo3Wrapper
	);

	nonFlexHorizontalPhotoRow2.setWidthFull();

	HorizontalLayout photo1aWrapper = createNonFlexHorizontalWrapper();
	HorizontalLayout photo2aWrapper = createNonFlexHorizontalWrapper();
	HorizontalLayout photo3aWrapper = createNonFlexHorizontalWrapper();

	Image photo1a;
	photo1aWrapper.add(
		photo1a = createRandomSquare900pxImage());

	Image photo2a;
	photo2aWrapper.add(
		photo2a = createRandomSquare900pxImage());

	Image photo3a;
	photo3aWrapper.add(
		photo3a = createRandomSquare900pxImage());

	photo1a.setWidthFull();
	photo2a.setWidthFull();
	photo3a.setWidthFull();

	nonFlexHorizontalPhotoRow2.add(
		photo1aWrapper,
		photo2aWrapper,
		photo3aWrapper
	);


	nonFlexHorizontalPhotoRow3.setWidthFull();

	VerticalLayout photo1bWrapper = createNonFlexVerticalWrapper();
	HorizontalLayout photo2bWrapper = createNonFlexHorizontalWrapper();
	VerticalLayout photo3bWrapper = createNonFlexVerticalWrapper();

	Image photo1b;
	photo1bWrapper.add(
		photo1b = createRandomSquare900pxImage()
		);

	Image photo2b;
	photo2bWrapper.add(
		photo2b = createRandomSquare900pxImage()
		);

	Image photo3b;
	photo3bWrapper.add(
		photo3b = createRandomSquare900pxImage()
		);

	photo1b.setWidthFull();
	photo2b.setWidthFull();
	photo3b.setWidthFull();

	nonFlexHorizontalPhotoRow3.add(
		photo1bWrapper,
		photo2bWrapper,
		photo3bWrapper
	);



}


}







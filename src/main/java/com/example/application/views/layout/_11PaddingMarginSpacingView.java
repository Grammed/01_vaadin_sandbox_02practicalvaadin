package com.example.application.views.layout;

import com.vaadin.flow.component.Component;
import com.vaadin.flow.component.Composite;
import com.vaadin.flow.component.button.Button;
import com.vaadin.flow.component.html.Paragraph;
import com.vaadin.flow.component.orderedlayout.HorizontalLayout;
import com.vaadin.flow.router.Route;

@Route("11_Padding-Margin-Spacing-Layout")
public class _11PaddingMarginSpacingView extends Composite<Component> {

@Override
protected Component initContent() {
	var layout = new HorizontalLayout();
	layout.getStyle().set("border", "1px solid");
	layout.setPadding(false);
	layout.setMargin(false);
	layout.setSpacing(false);
	layout.add(
		new Paragraph("Toggle:"),
		new Button("Padding", e ->
			                      layout.setPadding(!layout.isPadding())),
		new Button("Margin", e ->
			                     layout.setMargin(!layout.isMargin())),
		new Button("Spacing", e ->
			                      layout.setSpacing(!layout.isSpacing()))
	);
	return layout;
}
}


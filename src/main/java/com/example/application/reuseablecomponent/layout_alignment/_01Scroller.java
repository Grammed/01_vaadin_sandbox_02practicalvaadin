package com.example.application.reuseablecomponent.layout_alignment;

import com.vaadin.flow.component.Component;
import com.vaadin.flow.component.Composite;
import com.vaadin.flow.component.button.Button;
import com.vaadin.flow.component.orderedlayout.FlexComponent;
import com.vaadin.flow.component.orderedlayout.HorizontalLayout;
import com.vaadin.flow.component.orderedlayout.Scroller;
import com.vaadin.flow.component.orderedlayout.VerticalLayout;

public class _01Scroller extends Composite<Component>{
VerticalLayout holder = new VerticalLayout();
HorizontalLayout horizontalLayout = new HorizontalLayout();
VerticalLayout verticalLayout = new VerticalLayout();
Scroller horizontalScroller;
Scroller verticalScroller;

@Override
protected Component initContent() {

	for (int i = 0; i <= 100; i++) {
		horizontalLayout.add(new Button("Button " + i));
	}

	for (int i = 0; i <= 100; i++) {
		verticalLayout.add(new Button("Button " + i));
	}

	horizontalLayout.setAlignItems(FlexComponent.Alignment.CENTER);
	horizontalLayout.setPadding(true);
	horizontalLayout.getStyle().set("border", "1px dashed");// this shows on scroll

	horizontalScroller = new Scroller(horizontalLayout);
	horizontalScroller.setWidth("25rem");
	horizontalScroller.getStyle().set("border", "3px solid");

	verticalLayout.setAlignItems(FlexComponent.Alignment.CENTER);
	verticalLayout.setPadding(true);
	verticalLayout.getStyle().set("border", "1px dashed"); // this doesnot show on scroll

	verticalScroller = new Scroller(verticalLayout);
	verticalScroller.setHeight("25rem");
	verticalScroller.getStyle().set("border", "3px solid");

	holder.add(
		horizontalScroller,
		verticalScroller
	);

	holder.setPadding(true);

	return holder;
}


}

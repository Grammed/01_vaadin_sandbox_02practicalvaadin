package com.example.application.reuseablecomponent.layout_alignment;

import com.vaadin.flow.component.Component;
import com.vaadin.flow.component.Composite;
import com.vaadin.flow.component.button.Button;
import com.vaadin.flow.component.orderedlayout.FlexComponent;
import com.vaadin.flow.component.orderedlayout.FlexLayout;
import com.vaadin.flow.component.orderedlayout.VerticalLayout;

public class _03FlexLayout extends Composite<Component> {
HorizontalMessageReuseable directionMessage = new HorizontalMessageReuseable().setMessageReuseableText("FlexLayout.FlexDirection.COLUMN/ROW+_REVERSE");
VerticalLayout directionHolder = new VerticalLayout();
HorizontalMessageReuseable wrapMessage = new HorizontalMessageReuseable().setMessageReuseableText("FlexLayout.FlexWrap.WRAP/WRAP_REVERSE");
VerticalLayout wrapHolder = new VerticalLayout();
VerticalLayout mainHolder = new VerticalLayout();
FlexLayout flexLayout1 = new FlexLayout();
FlexLayout flexLayout2 = new FlexLayout();
FlexLayout flexLayout3 = new FlexLayout();
FlexLayout flexLayout4 = new FlexLayout();
FlexLayout flexLayout5 = new FlexLayout();
FlexLayout flexLayout6 = new FlexLayout();
FlexLayout flexLayout7 = new FlexLayout();
FlexLayout flexLayout8 = new FlexLayout();

@Override
protected Component initContent() {

	for (int i = 0; i <= 20; i++) {
		flexLayout1.add(new Button("Flex " + i));
	}

	for (int i = 0; i <= 20; i++) {
		flexLayout2.add(new Button("Flex " + i));
	}

	for (int i = 0; i <= 20; i++) {
		flexLayout3.add(new Button("Flex " + i));
	}

	for (int i = 0; i <= 20; i++) {
		flexLayout4.add(new Button("Flex " + i));
	}

	flexLayout1.setFlexDirection(FlexLayout.FlexDirection.COLUMN);
	flexLayout2.setFlexDirection(FlexLayout.FlexDirection.COLUMN_REVERSE);
	flexLayout3.setFlexDirection(FlexLayout.FlexDirection.ROW);
	flexLayout4.setFlexDirection(FlexLayout.FlexDirection.ROW_REVERSE);


	flexLayout1.getStyle().set("border", "dashed 1px");
	flexLayout2.getStyle().set("border", "dashed 1px");
	flexLayout3.getStyle().set("border", "dashed 1px");
	flexLayout4.getStyle().set("border", "dashed 1px");

	directionHolder.add(
		directionMessage,
		flexLayout1,
		flexLayout2,
		flexLayout3,
		flexLayout4
	);

	for (int i = 0; i <= 20; i++) {
		flexLayout5.add(new Button("Flex " + i));
	}

	for (int i = 0; i <= 20; i++) {
		flexLayout6.add(new Button("Flex " + i));
	}

	for (int i = 0; i <= 20; i++) {
		flexLayout7.add(new Button("Flex " + i));
	}

	for (int i = 0; i <= 20; i++) {
		flexLayout8.add(new Button("Flex " + i));
	}

	flexLayout5.setFlexDirection(FlexLayout.FlexDirection.COLUMN);
	flexLayout5.setFlexWrap(FlexLayout.FlexWrap.WRAP);
	flexLayout5.setAlignContent(FlexLayout.ContentAlignment.END);
	flexLayout5.setHeight("30rem");
	flexLayout5.setWidth("30rem");
	flexLayout6.setFlexDirection(FlexLayout.FlexDirection.COLUMN_REVERSE);
	flexLayout6.setFlexWrap(FlexLayout.FlexWrap.WRAP_REVERSE);
	flexLayout6.setAlignContent(FlexLayout.ContentAlignment.SPACE_BETWEEN);
	flexLayout6.setHeight("30rem");
	flexLayout6.setWidth("30rem");
	flexLayout7.setFlexDirection(FlexLayout.FlexDirection.ROW);
	flexLayout7.setFlexWrap(FlexLayout.FlexWrap.WRAP);
	flexLayout7.setAlignContent(FlexLayout.ContentAlignment.SPACE_AROUND);
	flexLayout7.setWidth("30rem");
	flexLayout7.setHeight("30rem");
	flexLayout8.setFlexDirection(FlexLayout.FlexDirection.ROW_REVERSE);
	flexLayout8.setFlexWrap(FlexLayout.FlexWrap.WRAP_REVERSE);
	flexLayout7.setAlignContent(FlexLayout.ContentAlignment.START);
	flexLayout8.setWidth("30rem");
	flexLayout8.setHeight("30rem");

	flexLayout5.getStyle().set("border", "dashed 1px");
	flexLayout6.getStyle().set("border", "dashed 1px");
	flexLayout7.getStyle().set("border", "dashed 1px");
	flexLayout8.getStyle().set("border", "dashed 1px");

	wrapHolder.setPadding(true);
	wrapHolder.setAlignItems(FlexComponent.Alignment.CENTER);
	wrapHolder.setSpacing(false);
	wrapHolder.setHeight("170rem");
	wrapHolder.setJustifyContentMode(FlexComponent.JustifyContentMode.EVENLY);

	wrapHolder.add(
		wrapMessage,
		flexLayout5,
		flexLayout6,
		flexLayout7,
		flexLayout8
	);

	mainHolder.add(
		wrapHolder,
		directionHolder
	);

	mainHolder.setPadding(true);


	return mainHolder;
}


}

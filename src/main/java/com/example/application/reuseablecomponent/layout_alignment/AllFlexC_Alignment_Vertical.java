package com.example.application.reuseablecomponent.layout_alignment;

import com.vaadin.flow.component.Component;
import com.vaadin.flow.component.Composite;
import com.vaadin.flow.component.button.Button;
import com.vaadin.flow.component.orderedlayout.FlexComponent;
import com.vaadin.flow.component.orderedlayout.VerticalLayout;

public class AllFlexC_Alignment_Vertical extends Composite<Component>{
VerticalLayout verticalLayout = new VerticalLayout();

@Override
protected Component initContent() {
	verticalLayout.add(
		new Button("START 1"),
		new Button("CENTER 2"),
		new Button("END 3"),
		new Button("STRETCH 4"),
		new Button("BASELINE 5")
	);
	verticalLayout.setAlignSelf(FlexComponent.Alignment.START, verticalLayout.getComponentAt(0));
	verticalLayout.setAlignSelf(FlexComponent.Alignment.CENTER, verticalLayout.getComponentAt(1));
	verticalLayout.setAlignSelf(FlexComponent.Alignment.END, verticalLayout.getComponentAt(2));
	verticalLayout.setAlignSelf(FlexComponent.Alignment.STRETCH, verticalLayout.getComponentAt(3));
	verticalLayout.setAlignSelf(FlexComponent.Alignment.BASELINE, verticalLayout.getComponentAt(4));
	verticalLayout.setMargin(false);
	verticalLayout.setPadding(true);
	verticalLayout.setHeight("18rem");
	verticalLayout.getStyle().set("border", "1px solid");
	return verticalLayout;
}


}
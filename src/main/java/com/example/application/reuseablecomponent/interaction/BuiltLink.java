package com.example.application.reuseablecomponent.interaction;

import com.helger.commons.io.stream.StringInputStream;
import com.vaadin.flow.component.Component;
import com.vaadin.flow.component.Composite;
import com.vaadin.flow.component.button.Button;
import com.vaadin.flow.component.html.Anchor;
import com.vaadin.flow.server.StreamResource;

import java.nio.charset.Charset;
import java.time.LocalTime;

//Tried setting up as component for multi type returns, but not used, all are anchor

public class BuiltLink extends Composite<Component> {
	private Component component;

@Override
protected Component initContent() {
	return component;
}

public Anchor getBasicBlogPlugLink() {
	component = new Anchor("https://www.programmingbrain.com", "visit my technical blog");
	Anchor anchor = (Anchor) component;
	anchor.setClassName("plug-link");
	return anchor;
}

public Anchor getButtonBlogPlugLink() {
	Button button = new Button("visit my technical blog");
	component = new Anchor("https://www.programmingbrain.com", button);
	Anchor anchor = (Anchor) component;
	anchor.setClassName("plug-link-button");
	return anchor;
}

public Anchor getTextLinkGeneratedAtRuntime() {
	Anchor textLink = new Anchor(new StreamResource(
		"text.txt",
		() -> {
			String content = "Time: " + LocalTime.now();
			return new StringInputStream(
				content, Charset.defaultCharset());
		}
	), "Server-generated text");
	textLink.setClassName("text-link");

	return textLink;
}



/*public Button getButtonWithLink() {
	Button button = new Button("not anchor, basic link");
	button.addClickListener(event -> {

	});*/
}
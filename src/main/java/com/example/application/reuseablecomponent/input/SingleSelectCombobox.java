package com.example.application.reuseablecomponent.input;

import com.example.application.views.component._01input.Department;
import com.vaadin.flow.component.Component;
import com.vaadin.flow.component.Composite;
import com.vaadin.flow.component.combobox.ComboBox;
import com.vaadin.flow.component.orderedlayout.HorizontalLayout;
import java.util.Collection;

public class SingleSelectCombobox extends Composite<Component> {
private ComboBox<Department> comboBox = new ComboBox<>();
private HorizontalLayout horizontalWrapper = new HorizontalLayout();

@Override
protected Component initContent() {

	return comboBox;
}

public ComboBox<Department> getComboBox() {

	comboBox.setLabel("Dept. (Hardcoded-list)");
	comboBox.setItems(
		new Department(1, "R&D_ts"),
		new Department(2, "Marketing_ts"),
		new Department(3, "Sales_ts"));

	return comboBox;
}

public ComboBox<Department> getComboBox(String label) {
	comboBox.setLabel(label);
	return comboBox;
}

public ComboBox<Department> getComboBox(String label, String postfix, Collection<Department> deptList) {
	comboBox.setLabel(label);
	comboBox.setItems(deptList);
	comboBox.setItemLabelGenerator(department -> {
		String text = department + " " + postfix;
		return text;
	});

	return comboBox;
}

public HorizontalLayout wrapComboBoxHorizontal() {
	this.horizontalWrapper.add(
		this.comboBox
	);
	setDefaultStyle();
	return horizontalWrapper;
}

public void setListItems() {
	this.comboBox.setItems(
	);
}

private void setDefaultStyle() {
	this.horizontalWrapper.getStyle().set("outline", "4px dashed");
	this.horizontalWrapper.setPadding(true);
	this.horizontalWrapper.setMargin(true);
}

}

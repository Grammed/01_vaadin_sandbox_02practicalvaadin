package com.example.application.views.layout;

import com.example.application.reuseablecomponent.layout_alignment._03FlexLayout;
import com.vaadin.flow.component.orderedlayout.VerticalLayout;
import com.vaadin.flow.router.Route;

@Route("05_Flex-Layout")
public class _05FlexLayoutView extends VerticalLayout {
VerticalLayout body = new VerticalLayout();
_03FlexLayout flexLayout = new _03FlexLayout();


public _05FlexLayoutView() {
	body.setMargin(false); // My experience so far, rely on padding, have unbalanced margins wherever margin is introduced..
	body.setPadding(true);
	body.setHeightFull();
	body.setWidthFull();
	body.add(
		flexLayout
	);

	add(
		body
	);

}


}

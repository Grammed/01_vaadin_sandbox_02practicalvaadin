package com.example.application.reuseablecomponent.interaction;

import com.vaadin.flow.component.Component;
import com.vaadin.flow.component.Composite;
import com.vaadin.flow.component.Text;
import com.vaadin.flow.component.contextmenu.ContextMenu;
import com.vaadin.flow.component.contextmenu.MenuItem;
import com.vaadin.flow.component.menubar.MenuBar;
import com.vaadin.flow.component.notification.Notification;
import com.vaadin.flow.component.orderedlayout.HorizontalLayout;

import java.util.List;

public class BuiltMenuBar extends Composite<Component> {
Component component;

protected Component initContent() {
	return component;
}

public MenuBar getBasicFileEdit(String id) {
	component = new MenuBar();
	MenuBar menuBar = (MenuBar) component;
	menuBar.setClassName("basic-file-menu");
	menuBar.setId(id);
	MenuItem file = menuBar.addItem("File");
	file.getSubMenu().addItem("New");
	file.getSubMenu().addItem("Open");
	MenuItem edit = menuBar.addItem("Edit");
	edit.getSubMenu().addItem("Copy");
	edit.getSubMenu().addItem("Paste");
	return menuBar;
}


public MenuBar getActionFileEdit(String id) {
	component = new MenuBar();
	MenuBar menuBar = (MenuBar) component;
	menuBar.setClassName("action-file-menu");
	menuBar.setId(id);
	MenuItem file = menuBar.addItem("File");
	file.getSubMenu().addItem("New");
	file.getSubMenu().addItem("Open");
	setChildItemAction(file.getSubMenu().getItems());
	MenuItem edit = menuBar.addItem(new Text("Edit"));
	edit.getSubMenu().addItem("Copy");
	edit.getSubMenu().addItem("Paste");
	setChildItemAction(edit.getSubMenu().getItems());
	return menuBar;
}

private void setChildItemAction(List<MenuItem> childItems) {
	for (MenuItem childitem : childItems) {
		childitem.addClickListener(click ->
			                           Notification.show(childitem.getText() + " has been pressed",
				                           4000, Notification.Position.MIDDLE));
	}

}

public HorizontalLayout getContextMenuHorizontalWrapped(String holderId, String contextMenuId, String rightClickText) {
	HorizontalLayout holder = new HorizontalLayout(new Text(rightClickText));
	holder.setClassName("action-context-menu");
	holder.setId(holderId);

	ContextMenu contextMenu = new ContextMenu(holder);
	contextMenu.setId(contextMenuId);
	contextMenu.addItem("Copy");
	contextMenu.addItem("Paste");
	setChildItemAction(contextMenu.getItems());

	return holder;
}
}

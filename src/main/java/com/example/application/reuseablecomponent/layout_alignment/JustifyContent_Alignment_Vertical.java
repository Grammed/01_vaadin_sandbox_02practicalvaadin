package com.example.application.reuseablecomponent.layout_alignment;
// #1 ORDERED LAYOUT ISSUE: https://github.com/vaadin/web-components/issues/804

import com.vaadin.flow.component.Component;
import com.vaadin.flow.component.Composite;
import com.vaadin.flow.component.button.Button;
import com.vaadin.flow.component.orderedlayout.FlexComponent;
import com.vaadin.flow.component.orderedlayout.HorizontalLayout;
import com.vaadin.flow.component.orderedlayout.VerticalLayout;

public class JustifyContent_Alignment_Vertical extends Composite<Component> {
VerticalLayout holder = new VerticalLayout();
HorizontalLayout messages = new HorizontalLayout();
HorizontalLayout holdsVerticalLayouts = new HorizontalLayout();

VerticalLayout innerHolder1 = new VerticalLayout();
VerticalLayout innerHolder2 = new VerticalLayout();
VerticalLayout innerHolder3 = new VerticalLayout();

VerticalLayout verticalLayout1 = new VerticalLayout();
VerticalLayout verticalLayout2 = new VerticalLayout();
VerticalLayout verticalLayout3 = new VerticalLayout();
VerticalLayout verticalLayout4 = new VerticalLayout();
VerticalLayout verticalLayout5 = new VerticalLayout();
VerticalLayout verticalLayout6 = new VerticalLayout();
VerticalLayout verticalLayout7 = new VerticalLayout();
VerticalLayout verticalLayout8 = new VerticalLayout();
VerticalLayout verticalLayout9 = new VerticalLayout();

@Override
protected Component initContent() {
	holder.setSpacing(false);
	messages.setSpacing(false);
	//#1
	innerHolder1.setSpacing(false);
	innerHolder2.setSpacing(false);
	innerHolder3.setSpacing(false);

	verticalLayout1.add(
		new Button("START 1")
	);
	verticalLayout2.add(
		new Button("CENTER 2")
	);
	verticalLayout3.add(
		new Button("END 3")
	);

	verticalLayout4.add(
		new Button("START 1")
	);
	verticalLayout5.add(
		new Button("CENTER 2")
	);
	verticalLayout6.add(
		new Button("END 3")
	);

	verticalLayout7.add(
		new Button("START 1")
	);
	verticalLayout8.add(
		new Button("CENTER 2")
	);
	verticalLayout9.add(
		new Button("END 3")
	);
	messages.add(

		new HorizontalMessageReuseable().setMessageReuseableText("FlexComponent.JustifyContentMode.EVENLY"),
		new HorizontalMessageReuseable().setMessageReuseableText("FlexComponent.JustifyContentMode.AROUND"),
		new HorizontalMessageReuseable().setMessageReuseableText("FlexComponent.JustifyContentMode.BETWEEN")

	);


	innerHolder1.add(
		//new HorizontalMessageReuseable().setMessageReuseableText("FlexComponent.JustifyContentMode.EVENLY"),
		verticalLayout1,
		verticalLayout2,
		verticalLayout3
	);

	innerHolder2.add(
	//	new HorizontalMessageReuseable().setMessageReuseableText("FlexComponent.JustifyContentMode.AROUND"),
		verticalLayout4,
		verticalLayout5,
		verticalLayout6
	);

	innerHolder3.add(
	//	new HorizontalMessageReuseable().setMessageReuseableText("FlexComponent.JustifyContentMode.BETWEEN"),
		verticalLayout7,
		verticalLayout8,
		verticalLayout9
	);

	holdsVerticalLayouts.add(
		innerHolder1,
		innerHolder2,
		innerHolder3
	);


	holder.add (
		messages,
		holdsVerticalLayouts
	);

	holder.setMargin(false);
	holder.setPadding(true);
	holder.setHeight("70rem");
	holder.getStyle().set("border", "3px solid");

	messages.setMargin(false);
	messages.setPadding(true);
	messages.setWidthFull();
	messages.setJustifyContentMode(FlexComponent.JustifyContentMode.AROUND);
	messages.getStyle().set("border", "3px dashed");

	holdsVerticalLayouts.setMargin(false);
	holdsVerticalLayouts.setPadding(true);
	holdsVerticalLayouts.setWidthFull();
	holdsVerticalLayouts.setHeight("60rem");
	holdsVerticalLayouts.getStyle().set("border", "3px dashed");



	// HERE
	innerHolder1.setJustifyContentMode(FlexComponent.JustifyContentMode.EVENLY);
	innerHolder2.setJustifyContentMode(FlexComponent.JustifyContentMode.AROUND);
	innerHolder3.setJustifyContentMode(FlexComponent.JustifyContentMode.BETWEEN);

	innerHolder1.getStyle().set("border", "1px dashed");
	innerHolder2.getStyle().set("border", "1px dashed");
	innerHolder3.getStyle().set("border", "1px dashed");

	innerHolder1.setMargin(false);
	innerHolder2.setMargin(false);
	innerHolder3.setMargin(false);
	innerHolder1.setPadding(true);
	innerHolder2.setPadding(true);
	innerHolder3.setPadding(true);

	verticalLayout1.setHeight("10rem");
	verticalLayout2.setHeight("10rem");
	verticalLayout3.setHeight("10rem");

	verticalLayout1.setMargin(false);
	verticalLayout2.setMargin(false);
	verticalLayout3.setMargin(false);

	verticalLayout1.setPadding(true);
	verticalLayout2.setPadding(true);
	verticalLayout3.setPadding(true);

	verticalLayout1.getStyle().set("border", "1px solid");
	verticalLayout2.getStyle().set("border", "1px solid");
	verticalLayout3.getStyle().set("border", "1px solid");

//HERE
	verticalLayout1.setJustifyContentMode(FlexComponent.JustifyContentMode.START);
	verticalLayout2.setJustifyContentMode(FlexComponent.JustifyContentMode.CENTER);
	verticalLayout3.setJustifyContentMode(FlexComponent.JustifyContentMode.END);

	verticalLayout4.setHeight("10rem");
	verticalLayout5.setHeight("10rem");
	verticalLayout6.setHeight("10rem");

	verticalLayout4.setMargin(true);
	verticalLayout5.setMargin(true);
	verticalLayout6.setMargin(true);

	verticalLayout4.setPadding(false);
	verticalLayout5.setPadding(false);
	verticalLayout6.setPadding(false);

	verticalLayout4.getStyle().set("border", "1px solid");
	verticalLayout5.getStyle().set("border", "1px solid");
	verticalLayout6.getStyle().set("border", "1px solid");

//HERE
	verticalLayout4.setJustifyContentMode(FlexComponent.JustifyContentMode.START);
	verticalLayout5.setJustifyContentMode(FlexComponent.JustifyContentMode.CENTER);
	verticalLayout6.setJustifyContentMode(FlexComponent.JustifyContentMode.END);

	verticalLayout7.setHeight("10rem");
	verticalLayout8.setHeight("10rem");
	verticalLayout9.setHeight("10rem");

	verticalLayout7.setMargin(false);
	verticalLayout8.setMargin(false);
	verticalLayout9.setMargin(false);

	verticalLayout7.setPadding(false);
	verticalLayout8.setPadding(false);
	verticalLayout9.setPadding(false);

	verticalLayout7.getStyle().set("border", "1px solid");
	verticalLayout8.getStyle().set("border", "1px solid");
	verticalLayout9.getStyle().set("border", "1px solid");

//HERE
	verticalLayout7.setJustifyContentMode(FlexComponent.JustifyContentMode.START);
	verticalLayout8.setJustifyContentMode(FlexComponent.JustifyContentMode.CENTER);
	verticalLayout9.setJustifyContentMode(FlexComponent.JustifyContentMode.END);

	return holder;
}

}
package com.example.application.reuseablecomponent.input;

import com.example.application.views.component._01input.Department;
import com.vaadin.flow.component.Component;
import com.vaadin.flow.component.Composite;
import com.vaadin.flow.component.orderedlayout.HorizontalLayout;
import com.vaadin.flow.component.radiobutton.RadioButtonGroup;

import java.util.Collection;

public class SingleSelectRadioButtonGroup extends Composite<Component> {
private RadioButtonGroup<Department> radioButtonGroup = new RadioButtonGroup<>();
private HorizontalLayout horizontalWrapper = new HorizontalLayout();

@Override
protected Component initContent() {

	return radioButtonGroup;
}

public RadioButtonGroup<Department> getRadioButtons() {
	radioButtonGroup.setLabel("Dept. (Hardcoded-list)");
	setButtons();

	return radioButtonGroup;
}

public RadioButtonGroup<Department> getRadioButtons(String label) {
	radioButtonGroup.setLabel(label);
	return radioButtonGroup;
}

public RadioButtonGroup<Department> getRadioButtons(String label, Collection<Department> deptList) {
	radioButtonGroup.setLabel(label);
	radioButtonGroup.setItems(deptList);
	radioButtonGroup.setRequired(true);
	radioButtonGroup.setErrorMessage("Just pick one already!");


	return radioButtonGroup;
}

public HorizontalLayout wrapRadioButtonsHorizontal() {
	this.horizontalWrapper.add(
		this.radioButtonGroup
	);
	setDefaultStyle();
	return horizontalWrapper;
}

public void setButtons() {
	radioButtonGroup.setItems(
		new Department(1, "R&D_ts"),
		new Department(2, "Marketing_ts"),
		new Department(3, "Sales_ts"));
}

private void setDefaultStyle() {
	this.horizontalWrapper.getStyle().set("outline", "4px dashed");
	this.horizontalWrapper.setPadding(true);
	this.horizontalWrapper.setMargin(true);
}


}

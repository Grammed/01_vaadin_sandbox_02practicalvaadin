package com.example.application.reuseablecomponent.interaction;

import com.vaadin.flow.component.Composite;
import com.vaadin.flow.component.button.Button;
import com.vaadin.flow.component.notification.Notification;
import java.time.LocalTime;


public class TimeButton extends Composite<Button> {
private Button button;

@Override
	protected Button initContent() {
		return button;
	}

public Button getBasicTimeButton() {
	button = new Button ("Time in the server, please");
	addServerTimeClickListener(button);
	return button;
}

public Button getOneShotTimeButton() {
	button = new Button ("Time in the server, please");
	//button.setDisableOnClick(true);
	addOneShotServerTimeClickListener(button);
	return button;
}

public Button getOneShotDisappearTimeButton() {
	button = new Button ("Time in the server, please");
	addOneShotDisappearServerTimeClickListener(button);
	return button;
}

private void addOneShotDisappearServerTimeClickListener(Button button) {
	button.addClickListener(event -> {
		event.getSource().setVisible(false);
		Notification.show("Sure, but just this once: " + LocalTime.now());
	});

}

private void addOneShotServerTimeClickListener(Button button) {
	button.addClickListener(event -> {
	event.getSource().setText("One use only");
	event.getSource().setEnabled(false);
	Notification.show("Sure, but just this once: " + LocalTime.now());
	});

}

private void addServerTimeClickListener(Button button) {
	button.addClickListener(clickEvent ->
		Notification.show("Sure " + LocalTime.now()
		));
}

}

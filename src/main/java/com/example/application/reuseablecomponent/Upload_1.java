package com.example.application.reuseablecomponent;

import com.vaadin.flow.component.Composite;
import com.vaadin.flow.component.notification.Notification;
import com.vaadin.flow.component.orderedlayout.FlexComponent;
import com.vaadin.flow.component.orderedlayout.HorizontalLayout;
import com.vaadin.flow.component.orderedlayout.VerticalLayout;
import com.vaadin.flow.component.upload.Receiver;
import com.vaadin.flow.component.upload.Upload;
import com.vaadin.flow.component.upload.receivers.FileBuffer;
import com.vaadin.flow.component.upload.receivers.MemoryBuffer;
import com.vaadin.flow.component.upload.receivers.MultiFileBuffer;
import com.vaadin.flow.component.upload.receivers.MultiFileMemoryBuffer;

import java.io.InputStream;
import java.util.Scanner;
import java.util.Set;

public class Upload_1 extends Composite<Upload> {
// Receiver interface: MemoryBuffer/MultiFileMemoryBuffer->Stores Data in Memory///FileBuffer/MultiFileBuffer Creates file File.createTempFile(String, String)

Upload upload = new Upload();
HorizontalLayout horizontalWrapper = new HorizontalLayout();
VerticalLayout verticalWrapper = new VerticalLayout();


protected Upload initContent() {
	return upload;
}

public Upload getUploadSingleFileInMemory() {
	MemoryBuffer receiver = new MemoryBuffer();
	this.upload.setReceiver(receiver);
	this.upload.setAcceptedFileTypes("text/plain"); //Based on MIME type/subtype: https://developer.mozilla.org/en-US/docs/Web/HTTP/Basics_of_HTTP/MIME_types  // Multipurpose Internet Mail Extensions
	return this.upload;
}

public Upload getUploadMultiFileInMemory() {
	MultiFileMemoryBuffer receiver = new MultiFileMemoryBuffer();
	this.upload.setReceiver(receiver);
	// puzzling how success event fired so
	this.upload.setMaxFiles(2);
	//this.upload.setAutoUpload(true);
	this.upload.setAcceptedFileTypes("text/plain");
	return this.upload;
}

public Upload getUploadSingleFileCreate() {
	FileBuffer receiver = new FileBuffer();
	this.upload.setReceiver(receiver);
	this.upload.setAcceptedFileTypes("text/plain");
	return this.upload;
}

public Upload getUploadMultiFileCreate() {
	MultiFileBuffer receiver = new MultiFileBuffer();
	this.upload.setReceiver(receiver);
	this.upload.setAcceptedFileTypes("text/plain");
	return this.upload;
}

public HorizontalLayout wrapHorizontal() {
	setDefaultStyleAndClass(1);
	this.horizontalWrapper.add(
		this.upload
	);
	return this.horizontalWrapper;

}

// Obv no need for return.
private void setDefaultStyleAndClass(int wrapType) {
	if(wrapType == 1) {
		this.horizontalWrapper.setClassName("basic-styled-upload-horizontal");
		this.horizontalWrapper.setPadding(true);
		this.horizontalWrapper.setMargin(true);
		this.horizontalWrapper.setDefaultVerticalComponentAlignment(FlexComponent.Alignment.CENTER);
		this.horizontalWrapper.getStyle().set("outline", "dashed 7px");
	} else if(wrapType == 2) {
		this.verticalWrapper.setClassName("basic-styled-upload-vertical");
		this.verticalWrapper.setPadding(true);
		this.verticalWrapper.setMargin(true);
		this.verticalWrapper.setDefaultHorizontalComponentAlignment(FlexComponent.Alignment.CENTER);
		this.verticalWrapper.getStyle().set("outline", "solid 7px");
	}
}

public VerticalLayout wrapVertical() {
	setDefaultStyleAndClass(2);
	this.verticalWrapper.add(
		this.upload
	);
	return this.verticalWrapper;
}

public void addOnSuccessBusinessLogic_01() {
	Receiver receiver = this.upload.getReceiver();

	if(receiver instanceof MemoryBuffer) {
		this.upload.addSucceededListener(succeededEvent -> {
			InputStream in = ((MemoryBuffer)receiver).getInputStream();
			long count = new Scanner(in).findAll("[Aa]").count();
			Notification.show("A x " + count + " times");
		});
	}else if (receiver instanceof MultiFileMemoryBuffer) {
		// todo Upload_1 not priority at time, MultiFileMemoryBuffer fire of completion on successful upload of specific number of files)
		//https://vaadin.com/forum/thread/17501896/multi-file-upload-in-flow-how-to-know-when-all-files-are-finished
		this.upload.addAllFinishedListener(allFinishedEvent -> {
			long count = 0;
			Set<String> allUploadedFiles = ((MultiFileMemoryBuffer) receiver).getFiles();
			for(String fileName: allUploadedFiles)	{
				InputStream forThisFile = ((MultiFileMemoryBuffer) receiver).getInputStream(fileName);
				count = count + new Scanner(forThisFile).findAll("[Aa]").count();
			}
			Notification.show("A x " + count + " times");
		});
	}

	//todo Upload_1 uploads that create files.

}
}

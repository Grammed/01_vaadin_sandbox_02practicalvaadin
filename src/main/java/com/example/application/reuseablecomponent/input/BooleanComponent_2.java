package com.example.application.reuseablecomponent.input;

import com.vaadin.flow.component.Component;
import com.vaadin.flow.component.Composite;
import com.vaadin.flow.component.checkbox.Checkbox;

public class BooleanComponent_2 extends Composite<Component> {
Checkbox checkbox = new Checkbox();

@Override
protected Component initContent() {


	return checkbox;
}

public BooleanComponent_2 createBasicCheckbox(String label, String ariaLabel ) {
	this.checkbox.setLabel(label);
	this.checkbox.setAriaLabel(ariaLabel);
	this.checkbox.setIndeterminate(true);
	setValueChangeListener(checkbox);
	return this;
}

private void setValueChangeListener(Checkbox checkbox) {
	   this.checkbox.addValueChangeListener(event -> {
		Boolean originalState = event.getOldValue();
		Boolean currentState = event.getValue();
		System.out.println("Notice: \noriginal state: " + originalState
			                   + "\nlast event state: " + currentState);
	});
}


public BooleanComponent_2 addBasic() {
	return this;
}

}
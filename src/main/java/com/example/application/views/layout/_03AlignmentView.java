package com.example.application.views.layout;

import com.example.application.reuseablecomponent.layout_alignment.AllFlexC_Alignment_Horizontal;
import com.example.application.reuseablecomponent.layout_alignment.AllFlexC_Alignment_Vertical;
import com.example.application.reuseablecomponent.layout_alignment.HorizontalMessageReuseable;
import com.example.application.reuseablecomponent.layout_alignment.JustifyContent_Alignment_Vertical;
import com.vaadin.flow.component.orderedlayout.VerticalLayout;
import com.vaadin.flow.router.Route;

@Route("03_Alignment-Layout")
public class _03AlignmentView extends VerticalLayout {
VerticalLayout body = new VerticalLayout();
AllFlexC_Alignment_Horizontal allFlexComponentHorizontalAlignments = new AllFlexC_Alignment_Horizontal();
AllFlexC_Alignment_Vertical allFlexComponentVerticalAlignments = new AllFlexC_Alignment_Vertical();
JustifyContent_Alignment_Vertical justifyContent_alignment_vertical = new JustifyContent_Alignment_Vertical();
HorizontalMessageReuseable message1 = new HorizontalMessageReuseable().setMessageReuseableText("" +
    "Shared space in a VerticalLayout? Don't let nested VL default to 100% height, none left for nested HorizontalLayouts.");
HorizontalMessageReuseable alignmentOnSecondary = new HorizontalMessageReuseable().setMessageReuseableText("" +
	 "ALIGNMENT ON SECONDARY AXIS Vertical==X Horizontal==Y");
HorizontalMessageReuseable message2 = new HorizontalMessageReuseable().setMessageReuseableText("" +
	 "verticalLayout.setAlignSelf(FlexComponent.Alignment.START, verticalLayout.getComponentAt(0))");
HorizontalMessageReuseable message3 = new HorizontalMessageReuseable().setMessageReuseableText("" +
    "horizontalLayout.setAlignSelf(FlexComponent.Alignment.START, verticalLayout.getComponentAt(0))");
HorizontalMessageReuseable alignmentOnPrimary = new HorizontalMessageReuseable().setMessageReuseableText("" +
	 "ALIGNMENT ON PRIMARY AXIS Vertical==Y Horizontal==X");



public _03AlignmentView() {
	body.setMargin(false); // My experience so far, rely on padding, have unbalanced margins wherever margin is introduced..
	body.setPadding(true);
	body.setHeightFull();
	body.setWidthFull();
	body.add(
		message1,
		alignmentOnSecondary,
		message2,
		allFlexComponentVerticalAlignments, // if this is allowed to default to full height other items can have no height
		message3,
		allFlexComponentHorizontalAlignments,
		alignmentOnPrimary,
		justifyContent_alignment_vertical

	);

	add(
		body
	);

}


}
